@extends('app')
@section('content')
  <section id="pageintro" class="hoc clear">
    <div> 
      
      <h2 class="heading">Online Quiz</h2>
      <p class="heading-paragraph">Sharpen your skills and polish your knowledges with interactive and performance-based exercises.</p>
      <footer><a class="btn" href="#heading">Play Now</a></footer>
      
    </div>
  </section>
  
</div>
<!-- End Top Background Image Wrapper -->
<div class="container">    
    <div class="container-fluid">
      <center><h1><b><a name="heading" style="font-family: impact;">Quiz Topics</a></b></h1>
      <p>Select the topics below to play</p></center> 
    </div>



    <div class="row">
      @foreach($subjects as $subject)
        <div class="col-sm-4">
          <a href="{{route('subjects.show', $subject->id)}} ">
          <div class="btn btn-default subjects">
            <h6 class="heading"">{{$subject->name}}</h6>
            <p>{{$subject->description}}</p>
         </div>
        </a>
        </div>
      @endforeach
    </div>
</div>



@endsection

