<div class="wrapper row5">
  <div id="copyright" class="hoc clear"> 
    
    <p class="fl_left">Copyright &copy; 2018 - All Rights Reserved - <a class="footlink" href="#">OES</a></p>
    <p class="fl_right">Designed and Developed by <a class="footlink" target="_blank" href="http://www.themenepal.com/" title="We Don't just build websites, we build your business">Theme Nepal</a></p>
    
  </div>
</div>

<a id="backtotop" href="#top"><i class="fa fa-chevron-up"></i></a>
<!-- JAVASCRIPTS -->

 <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-mobile/1.4.5/jquery.mobile.min.js"></script>
 <script src="{{ asset('public/js/custom.js')}}" ></script>

</body>
</html>