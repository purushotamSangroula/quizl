	<!DOCTYPE html>
<html lang="">
<head>
<title>Quiz</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
<link href="{{ asset('public/assets/layout/styles/layout.css')}}" rel="stylesheet" type="text/css" media="all">
 <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>

<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  
</head>
<body id="top">

<div class="bgded overlay"> 
  
 <div class="wrapper row1">
    <header id="header" class="hoc clear"> 
      
      <div id="logo" class="fl_left">
        <h1 class="heading"><a href="{{URL::to('/')}}">OES</a></h1>
      </div>
      <nav id="mainav" class="fl_right">
        <ul class="clear">
          <li class="active"><a href="{{URL::to('/')}}">Home</a></li>
          @if(!Auth::check())
            <li class=""><a href="{{URL::to('/')}}/login">Log In</a></li>
            <li class=""><a href="{{URL::to('/')}}/register">Sign Up</a></li>
          @else
            <li class=""><a href="#">Hello {{Auth::user()->firstname}}</a></li>
            <li class=""><a href="{{URL::to('/')}}/logout">Logout</a></li>
          @endif
        </ul>
      </nav>
      
    </header>
  </div>
</div>
