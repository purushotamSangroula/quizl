<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Admin Panel</title>

	<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

    <link href="{{ asset('public/assets/admin/assets/css/basic.css')}}" rel="stylesheet" />
    <link href="{{ asset('public/assets/admin/assets/css/custom.css')}}" rel="stylesheet" />
    <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css' />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-mobile/1.4.5/jquery.mobile.min.js"></script>
</head>
<body>
    
    <div id="wrapper">
        <nav class="navbar navbar-default navbar-cls-top " role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".sidebar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="{{route('admin.index')}}">OES</a>
            </div>

            <div class="header-right">
               <a href="{{URL::to('/')}}/logout" class="btn btn-danger" title="Logout"><i class="glyphicon glyphicon-off">  LogOut</i></a>
            </div>
        </nav>
        <nav class="navbar-default navbar-side" role="navigation">
            <div class="sidebar-collapse">
                <ul class="nav" id="main-menu">
                    <li>
                        <div class="user-img-div">
                            <!-- <img src="{{ asset('public/assets/admin/assets/img/user.png')}}" class="img-thumbnail" /> -->

                            <div class="inner-text">
                                {{Auth::user()->firstname}} {{Auth::user()->middlename}} {{Auth::user()->lastname}}
                            <br />
                                <small>Welcome to Admin Panel</small>
                            </div>
                        </div>

                    </li>


                    <li>
                        <a class="active-menu" href="{{URL::to('/')}}/admin"><i class="fa fa-dashboard "></i>Dashboard</a>
                    </li>
                   
                     <li>
                        <a href="#"><i class="glyphicon glyphicon-plus"></i>Add </a>
                         <ul class="nav nav-second-level collapse in">
                           
                             <li>
                                <a href="{{ route('admin.subjects.index')}}"><i class="glyphicon glyphicon-book"></i>Subject </a>
                            </li>
                             
                        </ul>
                        </ul>
                    </li>
                    
                </ul>
            </div>

        </nav>