@extends('admin.app')
@section('content')
        <!-- /. NAV SIDE  -->
        <div id="page-wrapper">
            <div id="page-inner">
                <div class="row">
                    <div class="col-md-12">
                        <h1 class="page-head-line">DASHBOARD</h1>
                        <h1 class="page-subhead-line">This is the list of subjects that are available now. </h1>
                    </div>
                </div>
                <!-- /. ROW  -->
                <?php $i = 0; $classes = ['mb-red', 'mb-dull' ,'mb-pink']; ?>
                <div class="row">
                    @foreach($subjects as $subject)
                        <div class="col-md-4">
                            <div class="main-box {{$classes[$i++]}}">
                                <a href="{{route('admin.subjects.show', $subject->id)}}">
                                    <i class="fa fa-bolt fa-5x"></i>
                                    <h5>{{$subject->name}}</h5>
                                </a>
                            </div>
                        </div>
                        <?php if($i== count($classes)) $i = 0; ?>
                    @endforeach
                </div>
            </div>
            <!-- /. PAGE INNER  -->
        </div>
        <!-- /. PAGE WRAPPER  -->
    
    <!-- /. WRAPPER  -->
@endsection