@extends('admin.app')
@section('content')
        <div id="page-wrapper">
            <div id="page-inner">
                <div class="row">
                    <div class="col-md-12">
                        <h1 class="page-head-line">{{$subject->name}}</h1><div style="float: right;">
                            
                        </div>
                        <h1 class="page-subhead-line">{{$subject->description}}</h1>

                    </div>
                </div>
                <!-- /. ROW  -->
                <div class="row">
            <div class="col-md-6 col-sm-6 col-xs-12">
               <div class="panel panel-info">
                        <div class="panel-heading">
                           SET LIST
                        </div>
                        <div class="panel-body">
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered table-hover">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                           
                                            <th>sets</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php $i =1; ?>
                                        @foreach($sets as $set)
                                            <tr>
                                                <td>{{$i++}}</td>
                                                <td><a href="{{route('admin.sets.show', $set)}}">Set {{$set->number}}</a></td>
                                                <td> <a href="{{route('admin.sets.edit', $set)}}" class="btn btn-primary"><i class="glyphicon glyphicon-edit"></i>Edit</a>
                                                 <a   
                                                    href="#"
                                                    onclick="
                                                    var result = confirm('Are you sure you wish to delete this set?');
                                                        if( result ){
                                                                event.preventDefault();
                                                                document.getElementById('delete-form').submit();
                                                        }
                                                            "
                                                            class="btn btn-danger">
                                                    <span class="glyphicon glyphicon-remove"></span> 
                                                    Delete
                                                </a>
                                                <form id="delete-form" action="{{ route('admin.sets.destroy',[$set->id]) }}" 
                                                  method="POST" style="display: none;">
                                                          <input type="hidden" name="_method" value="delete">
                                                          {{ csrf_field() }}
                                                </form>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                            </div>
                        </div>
                            </div>
<form method="post" action="{{route('admin.sets.store')}}" class="col-md-6 col-sm-6 col-xs-12">
               {{csrf_field()}}
               <input type="hidden" value="{{$subject->id}}" name="subject_id">
               <div class="panel panel-danger">
                        <div class="panel-heading">
                           Add Set
                        </div>
                        <div class="panel-body">
                   
                        <div class="form-group">
                            <label class="control-label col-lg-4">Set Number</label>
                            <label><input value="{{old('number')}}"  type="number" name="number"></label>
                        </div>
                        <hr>
                        
                        <div class="form-group">
                            <label class="control-label col-lg-4">No. of Question</label>
                            <label><input value="{{old('noofquestions')}}"  type="number" name="noofquestions"></label>    
                        </div>
                                        <hr>
                                    
                        <div class="form-group">
                            <label class="control-label col-lg-4">Total Mark</label>
                            <label><input value="{{old('total_mark')}}"  type="number" name="total_mark"></label>
                        </div>
                                    <hr>
                        <div class="form-group">
                            <label class="control-label col-lg-4">Pass Mark</label>
                            <label><input value="{{old('pass_mark')}}"  type="number" name="pass_mark"></label>
                        </div>
                                    <hr>
                        <div class="form-group">
                            <label class="control-label col-lg-4">Time in Minute</label>
                            <label><input value="{{old('time')}}"  type="number" name="time"></label>
                        </div>                        
                    </div>
                    
                    <hr>
                     <label><a href="#"> <button type="submit" class="btn btn-inverse"><i class="glyphicon glyphicon-plus"></i>Add</button></a></label>
                </div>

                </form>

                            
        
             <!--/.ROW-->
             

            </div>
            <!-- /. PAGE INNER  -->
        </div>
    <!-- /. WRAPPER  -->
</div> 
</div>

@endsection