@extends('admin.app')
@section('content')
        <div id="page-wrapper">
            <div id="page-inner">
                <div class="row">
                    <div class="col-md-12">
                        <h1 class="page-head-line">Subjects</h1><div style="float: right;">
                           
                        </div>
                        <h1 class="page-subhead-line">This is the list of subjects, you can add, edit or delete. </h1>

                    </div>
                </div>

                {{-- /. ROW --}}
                <div class="row">
            <div class="col-md-6 col-sm-6 col-xs-12">
               <div class="panel panel-info">
                        <div class="panel-heading">
                           SUBJECT LIST
                        </div>
                        <div class="panel-body">
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered table-hover">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                           
                                            <th>Subjects</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php $i =1; ?>
                                        @foreach($subjects as $subject)
                                            <tr>
                                                <td>{{$i++}}</td>
                                                <td><a href="{{route('admin.subjects.show', $subject->id)}}">{{$subject->name}}</a></td>
                                                <td> <a href="{{route('admin.subjects.edit', $subject->id)}}" class="btn btn-primary"><i class="glyphicon glyphicon-edit"></i>Edit</a>
                                                 <a onclick="
                                                    var result = confirm('Are you sure you wish to delete this Subject?');
                                                        if( result ){
                                                                event.preventDefault();
                                                                document.getElementById('delete-form').submit();
                                                        }
                                                            " class="btn btn-danger">
                                                    <i class="glyphicon glyphicon-remove"></i> 
                                                    Delete
                                                </a>
                                                <form id="delete-form" action="{{ route('admin.subjects.destroy',[$subject->id]) }}" 
                                                  method="POST" style="display: none;">
                                                          <input type="hidden" name="_method" value="delete">
                                                          {{ csrf_field() }}
                                                </form>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                            </div>
                        </div>
                            </div>
                <form method="post" class="col-md-6 col-sm-6 col-xs-12" action="{{route('admin.subjects.store')}}">
                    <div class="panel panel-danger">
                        <div class="panel-heading">
                           Add Subject
                        </div>
                        <div class="panel-body">
                   
                            <div class="form-group">
                                <label class="control-label col-lg-4">Subject Name</label>
                                <label><input value="{{old('name')}}" type="text" name="name"></label>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-lg-4">Description</label>
                                <label><textarea  type="text" name="description">{{ old('description') }}</textarea></label>
                            </div>
                            {{ csrf_field() }}
                             <label><a href="{{ route('admin.subjects.index')}}"> <button type="submit"class="btn btn-inverse"><i class="glyphicon glyphicon-plus"></i>Add</button></a></label>
                        </div>
                    </div>
                </form>
            {{-- /. PAGE INNER --}}
        </div>
        {{-- /. PAGE WRAPPER --}}
    </div>
    {{-- /. WRAPPER --}}
</div>

@endsection