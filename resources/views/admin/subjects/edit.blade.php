@extends('admin.app')
@section('content')
        <div id="page-wrapper">
            <div id="page-inner">
                <div class="row">
                    <div class="col-md-12">
                        <h1 class="page-head-line">Subjects</h1><div style="float: right;">
                           
                        </div>
                        <h1 class="page-subhead-line">This is the list of subjects, you can add, edit or delete. </h1>

                    </div>
                </div>

                {{-- /. ROW --}}
                <div class="row">
                <form method="post" class="col-md-6 col-sm-6 col-xs-12" action="{{route('admin.subjects.update' , ['id'=>$subject->id])}}">
                    <div class="panel panel-danger">
                        <div class="panel-heading">
                           Add Subject
                        </div>
                        <div class="panel-body">
                   
                            <div class="form-group">
                                <label class="control-label col-lg-4">Subject Name</label>
                                <label><input value="{{old('name' ,$subject->name )}}" type="text" name="name"></label>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-lg-4">Description</label>
                                <label><textarea  type="text" name="description">{{ old('description' , $subject->description) }}</textarea></label>
                            </div>
                            {{ csrf_field() }}
                        	{{ method_field('PUT')}} 

                             <label><a href="{{ route('admin.subjects.index')}}"> <button type="submit"class="btn btn-inverse"><i class="glyphicon glyphicon-plus"></i>Submit</button></a></label>
                        </div>
                    </div>
                </form>
            {{-- /. PAGE INNER --}}
        </div>
        {{-- /. PAGE WRAPPER --}}
    </div>
    {{-- /. WRAPPER --}}
</div>

@endsection