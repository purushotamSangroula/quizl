@extends('admin.app')
@section('content')
        <div id="page-wrapper">
            <div id="page-inner">
                <div class="row">
                    <div class="col-md-12">
                        <h1 class="page-head-line">Sets</h1><div style="float: right;">
                           
                        </div>
                        <h1 class="page-subhead-line">This is the list of sets, you can add, edit or delete. </h1>

                    </div>
                </div>
                {{-- /. ROW --}}
                <div class="row">
                <form method="post" action="{{route('admin.sets.update' , $set->id)}}" class="col-md-6 col-sm-6 col-xs-12">
                        {{ csrf_field() }}
                        {{ method_field('PUT')}} 
                       <div class="panel panel-danger">
                                <div class="panel-heading">
                                   Update Set
                                </div>
                                <div class="panel-body">
                            <div class="form-group">
                                <label class="control-label col-lg-4">Set Name</label>
                                <label><input value="{{old('number' ,$set->number)}}" type="text" name="number"></label>
                            </div>
                            <hr>
                            <div class="form-group">
                                <label class="control-label col-lg-4">No. of Question</label>
                                <label><input  value="{{old('noofquestions' ,$set->noofquestions)}}"  type="number" name="noofquestions"></label>    
                                            </div>
                                            <hr>
                                        
                                             <div class="form-group">
                                <label class="control-label col-lg-4">Total Mark</label>
                                <label><input value="{{old('total_mark' ,$set->total_mark)}}"  type="number" name="total_mark"></label>
                                            </div>
                                        <hr>
                                     <div class="form-group">
                                <label class="control-label col-lg-4">Pass Mark</label>
                                <label><input value="{{old('pass_mark' ,$set->pass_mark)}}"  type="number" name="pass_mark"></label>
                                            </div>
                                    <hr>
                        <div class="form-group">
                            <label class="control-label col-lg-4">Time in Minute</label>
                            <label><input value="{{old('time' ,$set->time)}}"  type="number" name="time"></label>
                        </div>  
                            
                            </div>
                            
                            <hr>
                             <label><a href="#"> <button type="submit" class="btn btn-inverse"><i class="glyphicon glyphicon-plus"></i>Update</button></a></label>
                                </div>

                        </form>
            {{-- /. PAGE INNER --}}
        </div>
        {{-- /. PAGE WRAPPER --}}
    </div>
    {{-- /. WRAPPER --}}
</div>

@endsection