@extends('admin.app')
@section('content')
        <div id="page-wrapper">
            <div id="page-inner">
                <div class="row">
                    <div class="col-md-12">
                        <h1 class="page-head-line">Set no: {{$set->number}}</h1><div style="float: right;">
                        
                        </div>
                        <h1 class="page-subhead-line">This is the list of questions, you can add, edit or delete.</h1>

                    </div>
                </div>
                <!-- /. ROW  -->
                <div class="row">
                    <div class="col-md-6 col-sm-6 col-xs-12">
                       <div class="panel panel-info">
                                <div class="panel-heading">
                                   Set of {{$subject}}
                                </div>
                                <div class="panel-body">
                                    <div class="table-responsive">
                                        <table class="table table-striped table-bordered table-hover">
                                            <thead>
                                                <tr>
                                                    <th>#</th>
                                                   
                                                    <th>Questions</th>
                                                    <th>Action</th>
                                                </tr>
                                            </thead>
                                            <?php $i = 1; ?>
                                            <tbody>
                                                @foreach($questions as $question)
                                                    @if($question-> questiontype_id != 1)
                                                        <tr>
                                                            <td>{{$i++}}</td>
                                                            <td>{{$question->question}}</td>
                                                            <td><a href="{{route('admin.questions.edit', $question)}}" class="btn btn-primary"><i class="glyphicon glyphicon-edit"></i>Edit</a>
                                                     <a   
                                                        href="#"
                                                        onclick="
                                                        var result = confirm('Are you sure you wish to delete this question?');
                                                            if( result ){
                                                                    event.preventDefault();
                                                                    document.getElementById('delete-form{{$question->id}}').submit();
                                                            }
                                                                "
                                                                class="btn btn-danger btn-xs">
                                                        <span class="glyphicon glyphicon-remove"></span> 
                                                        Delete
                                                    </a>
                                                    <form id="delete-form{{$question->id}}" action="{{ route('admin.questions.destroy',$question->id) }}" 
                                                      method="POST" style="display: none;">
                                                              <input type="hidden" name="_method" value="delete">
                                                              {{ csrf_field() }}
                                                    </form>
                                                    </td>
                                                        </tr>
                                                    @endif
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                        </div>
                    </div>
                    <form method="post" action="{{route('admin.questions.store')}}" class="col-md-6">
                        {{csrf_field()}}
                        <input type="hidden" name="set_id" value="{{$set->id}}">
                       <div class="panel panel-default">
                            <div class="panel-heading">
                               Add Questions
                            </div>
                            <div class="panel-body">
                                <div class="form-group">
                                    <label class="control-label col-lg-4">Question</label>
                                    <textarea name="question" style="width:65%;">{{old('question')}}</textarea>
                                </div>                         
                                <hr>
                                <div class="form-group">
                                    <label class="control-label col-lg-4">Type</label>
                                    <div class="btn-group">
                                        <select name="questiontype_id" data-toggle="dropdown" id='question_type_select' class="btn dropdown-toggle">Select Type <span class="caret"></span>
                                            @foreach($question_types as $question_type)
                                            <option value="{{$question_type->id}}"
                                                @if($question_type->id == old('questiontype_id') ) selected @endif 
                                                @if($question_type->id == 1 ) disabled @endif 
                                                >{{$question_type->name}}</option>
                                            @endforeach
                                        </select>
                                        
                                    </div>
                                </div>
                                <hr>
                                <div class="form-group">
                                    <label class="control-label col-lg-4">Weightage</label>
                                    <label><input type="number" name="weightage" value="{{old('weightage')}}"></label>
                                </div>
                                <hr>
                                <div class="form-group no-of-options">
                                    <label class="control-label col-lg-4">Number of options</label>
                                    <label><input id="noofoptions" type="number" name="noofoptions" value="{{old('noofoptions')}}"></label>
                                </div>
                                <hr>
                            </div>
                                <div id="mcq_question">
                                    <div class="form-group">
                                        <label class="control-label col-lg-4">Options</label>
                                       <label>
                                        <ol id="answer_options">
                                        </ol> 
                                      </label>
                                    </div>
                                    <div class="form-group">
                                    <label class="control-label col-lg-4">Correct Answer(s)</label>
                                        <label><div id = "correct_answers" >
                                            
                                        </div></label>
                                    </div>
                                </div>
                                <div id="match-the-following" >
                                    <div class="form-group" >
                                        <table class="table table-hover text-center">
                                            <thead>
                                              <tr>
                                                <th class="text-center">Left</th>
                                                <th class="text-center">Answer</th>
                                                <th class="text-center"><span>S.No.</span></th>
                                                <th class="text-center">Right</th>
                                              </tr>
                                            </thead>
                                            <tbody id="match-the-following-tbody">
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div id="question-with-subquestions" >
                                    <div class="form-group">
                                        <label class="control-label col-lg-4">No of Sub Question</label>
                                        <label><input type="number" id="numofsubq" name="numofsubq"></label>
                                    </div>
                                    <hr>
                                    <div id="sub-questions">
                                    </div>
                                </div>
                                <label class="container fluid"><a href="#"> <button type="submit" class="btn btn-inverse"><i class="glyphicon glyphicon-plus"></i>Add</button></a></label>
                        </div> 
                    </form>
                 </div>
            <!-- /. PAGE INNER  -->
        </div>
        <!-- /. PAGE WRAPPER  -->
    </div>
@endsection