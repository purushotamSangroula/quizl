@extends('admin.app')
@section('content')
        <div id="page-wrapper">
            <div id="page-inner">
                <div class="row">
                    <div class="col-md-12">
                        <h1 class="page-head-line">Question</h1><div style="float: right;">
                           
                        </div>
                        <h1 class="page-subhead-line">Edit this question. <span class="alert alert-success">(Please! do not change Question type. If you want another type then just delete this one and create new one.)<span></h1>

                    </div>
                </div>

                {{-- /. ROW --}}
                <div class="row">
                    <form method="post" action="{{route('admin.questions.update', ['id'=>$question->id])}}" class="col-md-6">
                       <div class="panel panel-default">
                            <div class="panel-heading">
                               Edit Questions 
                            </div>
                            <div class="panel-body">
                                <div class="form-group">
                                    <label class="control-label col-lg-4">Question</label>
                                    <textarea name="question" style="width:65%;">{{old('question' ,$question->question )}}</textarea>
                                </div>                         
                                <hr>
                                <div class="form-group">
                                    <label class="control-label col-lg-4">Type </label>
                                    <div class="btn-group">
                                        <select name="questiontype_id" data-toggle="dropdown" id='question_type_select' class="btn dropdown-toggle">Select Type <span class="caret"></span>
                                            @foreach($question_types as $question_type)
                                            <option @if($question_type->id != $questiontype_id) disabled @endif 
                                                    @if($question_type->id == 1 ) disabled @endif 
                                                value="{{$question_type->id}}">{{$question_type->name}}</option>
                                            @endforeach
                                        </select>
                                        
                                    </div>
                                </div>
                                <hr>
                                <div class="form-group">
                                    <label class="control-label col-lg-4">Weightage</label>
                                    <label><input value="{{old('weightage' , $question->weightage) }}" type="number" name="weightage"></label>
                                </div>
                                <hr>
                                <div class="form-group no-of-options">
                                    <label class="control-label col-lg-4">Number of options</label>
                                    <label><input value="{{old('noofoptions' , $question->noofoptions) }}"  id="noofoptions" type="number" name="noofoptions"></label>
                                </div>
                                <hr>
                                @if($questiontype_id == 3 )
                                    <div id="mcq_question">
                                        <div class="form-group">
                                            <label class="control-label col-lg-4">Options</label>
                                           <label>
                                            <ol id="answer_options">
                                                <?php $i = 1; ?>
                                                @foreach($answers as $answer)
                                                    <li id='ans{{$i}}' ><label><input 
                                                        value= "{{ old('ans'.$i , $answer)}}" type='text' name='ans{{$i++}}'></label></li>
                                                @endforeach
                                            </ol> 
                                          </label>
                                        </div><hr>
                                        <div class="form-group">
                                        <label class="control-label col-lg-4">Correct Answer(s)</label>
                                            <div id = "correct_answers" >
                                                <?php $i = 1; ?>
                                                @foreach($ansObject as $ao)
                                                    <?php $id = $ao->id?>
                                                    <li class="list-unstyled"><input @if( in_array( $id  , $rightAnswers) ) checked @endif
                                                    type="checkbox" name="correct_answers[]" value ="{{old('correct_answers[]' , $i)}}" > {{$i++}}</li>
                                                @endforeach
                                            </div>
                                        </div>
                                    </div>
                                @elseif($questiontype_id == 4 )
                                    <div id="match-the-following">
                                        <div class="form-group">
                                        <label class="control-label col-lg-4">Correct Answer(s)</label>
                                            <div id="match-the-following" >
                                                <div class="form-group">
                                                    <table class="table table-hover text-center">
                                                        <thead>
                                                          <tr>
                                                            <th class="text-center">Left</th>
                                                            <th class="text-center">Answer</th>
                                                            <th class="text-center"><span>S.No.</span></th>
                                                            <th class="text-center">Right</th>
                                                          </tr>
                                                        </thead>
                                                        <tbody id="match-the-following-tbody">
                                                            <?php $i =1; $j = 1; $correctAnsArr = $rightAnswers->all();  ?>
                                                            @foreach($childQuestions as $childQuestion )
                                                              <tr>
                                                                <td><textarea class="form-control" type="text" name='subquestions[]'   >{{old('subquestions[]' , $childQuestion->question)}}
                                                                </textarea></td>
                                                                <td><select name='correct_answers[]'  class="btn dropdown-toggle">
                                                                    @foreach($childQuestions as $a)
                                                                        <option value="{{$j}}" 
                                                                            @if($childQuestion->rightanswers()->first()->dropdown_number == $j) selected @endif >{{$j}}</option>
                                                                        <?php $j++; ?>
                                                                    @endforeach
                                                                </select></td>
                                                                <td><span class='serial_number'>{{ $i++ }}</span></td>
                                                                <td><textarea name='subanswers[]' class="form-control" type="text"   >{{old('subquestions[]' , $childQuestion->answers()->first()->answer )}}</textarea>
                                                                </td>
                                                              </tr>
                                                              <?php $j++; $j =1 ; ?>
                                                            @endforeach
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @elseif($questiontype_id == 5 )
                                    <div id="question-with-subquestions" >
                                        <div class="form-group">
                                            <label class="control-label col-lg-4">No of Sub Question</label>
                                            <label><input type="number" id="numofsubq" name="numofsubq" value="{{old('numofsubq' , $question->noofoptions) }}"></label>
                                        </div>
                                        <hr>
                                        <div id="sub-questions">
                                            <?php $i = 1 ; //dd($childRightAnswersDropdowns); ?>
                                            @foreach( $childQuestions as $childQuestion )
                                                <div class="sub-question" style="border-style: solid; border-radius: 5px; border-color: #ff0000; padding: 5px;">
                                                    <div class="form-group">
                                                      <label class="control-label col-lg-4">Sub Question</label>
                                                        <textarea name="subquestions[{{$i}}][]" style="width:60%;">{{old('subquestions[$i][]' , $childQuestion->question )}}</textarea>
                                                    </div>
                                                    <hr>
                                                    <div class="form-group">
                                                      <label class="control-label col-lg-4">No of Answers</label>
                                                        <input type="number" class="number-of-answers" name="number-of-answers[{{$i}}]" value="{{old('number-of-answers[$i]' , $childQuestion->noofoptions  )}}" >
                                                    </div>
                                                    <div class="form-group">
                                                      <label class="control-label col-lg-4">Answers</label>
                                                        <label>
                                                            <ol class="ol-answers">
                                                                @foreach($answers[$childQuestion->id] as $answer)
                                                                    <li><input type="text" name="answers[{{$i}}][]" value="{{old('answers[$i][]' , $answer->answer)}}"></li>
                                                                @endforeach
                                                            </ol> 
                                                          </label>
                                                    </div>
                                                    <hr>
                                                    <div class="form-group correct-answer">
                                                        <label class="control-label col-lg-4">Correct Answer</label>
                                                            <label>
                                                               <select name= correct-answer[{{$i}}] >
                                                                   @for($j = 1 ; $j <= $childQuestion->noofoptions; $j++)
                                                                    <option @if($j == $childRightAnswersDropdowns[$i-1]) selected @endif value="{{$j}}">{{$j}}</option>
                                                                   @endfor
                                                               </select>
                                                            </label>
                                                   </div>
                                                   <hr>
                                                </div>
                                                <?php $i++; ?>
                                            @endforeach
                                        </div>
                                    </div>
                                @endif
                                </div>
                                <label><a href="#"> <button type="submit" class="btn btn-inverse"><i class="glyphicon glyphicon-plus"></i>Update</button></a></label>
                        </div> 
                            {{ csrf_field() }}
                        	{{ method_field('PUT')}} 
                    </form>
            {{-- /. PAGE INNER --}}
        </div>
        {{-- /. PAGE WRAPPER --}}
    </div>
    {{-- /. WRAPPER --}}
</div>

@endsection
