@extends('app1')
@section('content')

  <section id="breadcrumb" class="hoc clear"> 
    
    <h6 class="heading" style="color: white;">Sets</h6>
    
    <ul style="color: white;">
      <li><a href="{{URL::to('/')}}">Home</a></li>
      <li><a href="{{route('subjects.show' , $subject->id)}}">{{$subject->name}}</a></li>
      <li><a href="#">Set {{$set->number}}</a></li>
    </ul>
    
  </section>
</div>
<?php $img_url = asset('public/assets/images/demo/backgrounds/ahaha.jpg');?>
  <div  style="background-image:url({{$img_url}});"><div class="container"> 
    <!-- div body -->
    <blockquote>
      <div class="heading green">Questions</div>
      Subject: <i><b>{{$subject->name}}</b></i>
    <footer>Set No: {{$set->number}}</footer></blockquote>
         <b>Total Questions: {{$totalQuestions}}</b><b style="float:right;">Full mark:{{$set->total_mark}} <br> Pass Mark:{{$set->pass_mark}}</b>
    <center><a class="btn btn-default" id="Start" href="#hidden" data-toggle="collapse" onclick="style='display:none'">Start</a></center>
            <div id="hidden" class="collapse">
  <form id="exam-form" method="post" action="{{route('results.store')}}">
<br><br>
      <div id="timer"><div id='tiles'></div></div><span class="out-of-time"></span>
<hr>
       <b class="text-danger unanswered-questions">Status:</b> <div id="unanswered-questions"></div>
        <ul id="question_answers">
          <?php $i =1; ?>
          @foreach($questions as $question)
            @if($question->questiontype_id == 3 )
                <div class="one_question">
                  <strong><li class="question" style="list-style: none;"><span class="question-number">{{$i++}}</span>. {{$question->question}}<div class="badge" style="float: right;background-color: #a2b70d;">Weightage: {{$question->weightage}}</div></li></strong><br>
                  <big><input class="numberOfAnswers" type="hidden" id="{{$question->id}}" value="{{count($question->rightanswers)}}">
                 @foreach($question->answers as $answer)
                  <label class="checkboxes"><input value="{{$answer->id}}" type="checkbox" name="{{$question->id}}[]"><span class="label-text">{{$answer->answer}}</span></label><br>
                  @endforeach
                </big>
                  
                </div>
            @elseif( $question->questiontype_id == 4 )
                <?php
                  $childQuestions = $question->getChildrenQuestions($question->id) ;
                ?>
                <div class="one_question">
                  <strong>
                    <li class="question" style="list-style: none;"><span class="question-number">{{$i++}}</span>. {{$question->question}}
                      <div class="badge" style="float: right;background-color: #a2b70d;">Weightage: {{$question->weightage}}</div>
                    </li>
                  </strong><br>
                  <!--match the following-->
                  <div class="mtf">
                  <div class="col-sm-6">
                      <li style="list-style: none;"><b>Ques</b></li>
                    @foreach($childQuestions as $childQuestion)
                      <li class="draggable1" draggable="true">{{$childQuestion->question}}</li>
                    @endforeach
                  </div>

                  <div class="col-sm-6">
                    <li style="list-style: none;"><b>Move to Order Me</b></li>
                    @foreach($childQuestions as $childQuestion)
                      <div class="draggable" draggable="true">
                        <li>{{$childQuestion->answers()->first()->answer}}</li>
                        <input type="checkbox" name="{{$question->id}}[]" value="{{$childQuestion->answers()->first()->id}}" style="display: none;" checked>
                      </div>
                    @endforeach
                  </div>
                </div>
                  <!--end of match the following-->
                </div>
            @elseif( $question->questiontype_id == 5 )
                <?php
                  $childQuestions = $question->getChildrenQuestions($question->id) ;
                  $j = 1;
                ?>
                <div class="one_question">
                  <div class="question"><strong><span class="question-number">{{$i++}}. </span>{{$question->question}}</strong></div>
                  <div class="badge" style="float: right;background-color: #a2b70d;">Weightage: {{$question->weightage}}</div><br>
                  <p class="container-fluid lead"><small>
                    @foreach($childQuestions as $childQuestion)
                      <span class="subquestions-sn">{{$j++}}. </span>{{$childQuestion->question}} 
                        <?php $ansss = $childQuestion->answers; ?>
                        <select class="btn btn-default" name="{{$question->id}}[]" >
                        @foreach( $ansss as $anss )
                          <option value="{{$anss->id}}">{{$anss->answer}}</option>
                        @endforeach
                        </select><br>
                    @endforeach
                  </small>
                  </p>
                </div>
            @endif
          @endforeach
            <div class="flag-button " id="flag-button" ><i class="fa fa-flag"></i></div>
          <div class="navigation-bar"><div class="btn btn-default pn" id="navigation-prev"><span class="glyphicon glyphicon-backward"></span> PREV</div><div class="btn btn-default pn" id="navigation-next">NEXT <span class="glyphicon glyphicon-forward"></span></div></div>
        </ul>
      {{csrf_field()}}
        <input type="hidden" value="{{$subject->id}}"  name="subject_id">
        <input type="hidden" value="{{$set->id}}"   name="set_id">
        <input type="hidden" value="1"    name="user_id">
     <button id="exam-submit" onclick="submitFormOkay = true;" type="submit" class="btn">Submit <span class="glyphicon glyphicon-check"></span></button>
    </form>
               </div>
      </div>
   </div>

@endsection
<script>
    var totalQuestions = {{$totalQuestions}};
    var timeForSet = {{$set->time}};
    var quizPage = true;
  /*stop from refreshing result page*/
    var submitFormOkay = false;
  window.onbeforeunload = function(){
    if (!submitFormOkay) {
        return "You are about to leave this order form. You will lose any information...";
    }
  }
</script>