@extends('app1')
@section('content')
  <section id="breadcrumb" class="hoc clear"> 
    
    <h6 class="heading" style="color: white;">Sets</h6>
    
    <ul style="color: white;">
      <li><a href="{{URL::to('/')}}">Home</a></li>
      <li><a href="#">{{$subject->name}}</a></li>
    </ul>
    
  </section>
</div>
<?php $img_url = asset('public/assets/images/demo/backgrounds/ahaha.jpg');?>
<div class="wrapper row3" style="background-image:url({{$img_url}});">
  <div class="container"> 
    <!-- div body -->
    
    
      
      <h2 class="heading left-border">Sets for "{{$subject->name}}"</h2>
      <p><big>These are the availabe list of sets. Please click any one to play.</big></p>
      @guest
          <center><div class="text-danger" style="font-size: 24px;"> Please Login to play quiz.</div></center>
      @endguest
      <div class="container-fluid">
	      @foreach($sets as $set)
    	   <a href="{{route('sets.show' , ['id'=>$set->id])}}">
          <div class="col-sm-3">
            <div class="btn btn-default sets list" style="width: 100%; margin-bottom: 20px; padding: 10px;">Set {{$set->number}}</div>
          </div>
        </a>
    	  @endforeach
      </div>

    </div>
    
    <div class="clear"></div>
</div>
@endsection