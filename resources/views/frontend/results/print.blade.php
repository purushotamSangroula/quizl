@extends('app2')
@section('content')
<?php $img_url = asset('public/assets/images/demo/backgrounds/ahaha.jpg');?>
<div style="background-image:url({{$img_url}});">
<div class="container result" >
  <a class="btn btn-primary fl_right" onclick="myFunction()"><span class="glyphicon glyphicon-print print"></span> Print
    </a>
  <div class="resulttab"><center><h3><u>OES Quiz Marksheet</u></h3></center>

  <b>Student Name: {{auth::user()->firstname}} <span class="fl_right">Pass Mark:40</span><br> 
  <span class="fl_right">Subject: {{$subject->name}}</span>Set No.: {{$set->number}}</b>      
  <table class="table table-striped">
    <thead>
      <tr>
        <th class="col-sm-1">#</th>
        <th class="col-sm-8">Question</th>
        <th class="col-sm-1">Result</th>
        <th class="col-sm-1">Awarded</th>
        <th class="col-sm-1">Points</th>
      </tr>
    </thead>
    <tbody>
      <?php $i = 1; ?>
      @foreach($questions as $question)
        @if($question->questiontype_id == 3 )
          <tr style="text-align: center;">
            <td style="text-align: right;">{{$i++}}</td>
            <td style="text-align: left;">{{$question->question}}</td>
            <td>
              @if($currentResults["printResult"][$question->id])
                <span class="glyphicon glyphicon-ok green"></span>
              @else
                <span class="glyphicon glyphicon-remove red"></span>
              @endif
            </td>
            <td>@if($currentResults["printResult"][$question->id]) {{$question->weightage}} @else 0 @endif</td>
            <td>{{$question->weightage}}</td>
          </tr>
        @elseif( $question->questiontype_id ==4  )
          <tr style="text-align: center;">
            <td style="text-align: right;">{{$i++}}</td>
            <td style="text-align: left;">{{$question->question}}</td>
            <td>
              @if($currentResults["printResult"][$question->id])
                <span class="glyphicon glyphicon-ok green"></span>
              @else
                <span class="glyphicon glyphicon-remove red"></span>
              @endif
            </td>
            <td>@if($currentResults["printResult"][$question->id]) {{$question->weightage}}  @else 0 @endif</td>
            <td>{{$question->weightage}}</td>
          </tr>
        @elseif( $question->questiontype_id == 5  )
          <tr style="text-align: center;">
            <td style="text-align: right;">{{$i++}}</td>
            <td style="text-align: left;">{{$question->question}}</td>
            <td>
              @if($currentResults["printResult"][$question->id])
                <span class="glyphicon glyphicon-ok green"></span>
              @else
                <span class="glyphicon glyphicon-remove red"></span>
              @endif
            </td>
            <td>@if($currentResults["printResult"][$question->id]) {{$question->weightage}}  @else 0 @endif</td>
            <td>{{$question->weightage}}</td>
          </tr>
        @endif
      @endforeach
    </tbody>
    <thead>
      <tr>
        <th class="col-sm-1">Total</th>
        <th class="col-sm-8"></th>
        <?php $om = $result->obtainedmark; $pm = $set->pass_mark; ?>
        <th class="col-sm-1" style="text-align: center;">
          @if( $om >= $pm )<span style="color: green;">Pass </span><span class="glyphicon glyphicon-ok green"></span> 
          @else Fail <span class="glyphicon glyphicon-remove red"></span> @endif
        </th>
        <th class="col-sm-1" style="text-align: center;">{{ $result->obtainedmark}}</th>
        <th class="col-sm-1" style="text-align: center;">{{ $set->total_mark }}</th>
      </tr>
    </thead>
  </table>
</div>
</div>
</div>
@endsection

<script>
function myFunction() {
    window.print();
}
</script>