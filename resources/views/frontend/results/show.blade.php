@extends('app1')
@section('content')
  <section id="breadcrumb" class="hoc clear"> 
    
    <h6 class="heading" style="color: white;">Sets</h6>
    
    <ul style="color: white;">
      <li><a href="{{URL::to('/')}}">Home</a></li>
      <li><a href="{{route('subjects.show' , $subject->id)}}">{{$subject->name}}</a></li>
      <li><a href="{{route('sets.show' , $set->id)}}">Set {{$set->number}}</a></li>
      <li><a href="#">Results</a></li>
    </ul>
  </section>
</div>
<?php $img_url = asset('public/assets/images/demo/backgrounds/ahaha.jpg');?>
<div class="wrapper row3" style="background-image:url({{$img_url}});">
  <main class="container clear"> 

    <!-- main body -->
    <blockquote><div class="heading green">Results</div>
    <?php 
      $om = $result->obtainedmark; 
      $totalMark = $set->total_mark;
    	
      if($om >= (60*$totalMark)/100)
    	$ans_col = "green";

    	elseif($om >= (40*$totalMark)/100) 
    		$ans_col= "orange";

    	else
    		$ans_col = "red";
    ?>
    <label class="fl_right {{$ans_col}}">Score: {{$om}}</label>
    
    <div>Subject: <b><i>{{$subject->name}}</i></b></div>
    <footer>Set No: {{$set->number}}</footer>
    <a class="btn btn-primary fl_right" href="{{route('result.print' , $result->id)}}"><span class="glyphicon glyphicon-book

"></span> Marksheet
    </a>
  </blockquote> <hr>
    <form >
        <ol>
            @foreach($questions as $question)
              @if($question->questiontype_id != 1)
                <strong><li class="question">{{$question->question}}</li><div class="badge @if($currentResults['printResult'][$question->id]) weight-rightans @else weight-wrongans @endif"  >Weightage: {{$question->weightage}}</div></strong>
              @endif
              @if($question->questiontype_id == 3)
                <?php
                $i =1; 
                ?>
                    @foreach($question->answers as $answer)
                      <big><div class="checkbox disabled"><label class="
                                    @if(array_key_exists($answer->id, $performance))
                                      @if( $performance[$answer->id]== true) rightans
                                      @else wrongans
                                      @endif
                                    @endif
                       ">
                      	<input value="{{$i++}}" type="checkbox" name="{{$question->id}}" disabled><span class="label-text">{{$answer->answer}}</span>
                      </label></div></big>
                    @endforeach
                  <hr>
                @elseif($question->questiontype_id == 4)
                  @if($currentResults["matchTheFollowing"][$question->id]["result"])
                    <div class="right-match">You did it correctly.</div>
                  @else
                    <div class="wrong-match">You did it wrong.</div>
                  @endif
                @elseif($question->questiontype_id == 5)
                  @if($currentResults["questionWithSubQuestions"][$question->id]["result"])
                    <div class="right-match">You did it correctly.</div>
                  @else
                    <div class="wrong-match">You did it wrong.</div>
                  @endif
                @endif
             @endforeach
        </ol>
      {{csrf_field()}}
        <input type="hidden" value="{{$subject->id}}"  name="subject_id">
        <input type="hidden" value="{{$set->id}}"   name="set_id">
        <input type="hidden" value="1"    name="user_id">
    </form>
    </div>
    
@endsection
<style type="text/css">
	.rightans{
		color:green;
	}
	.wrongans{
		color:red;
	}
</style>
