
<div class="wrapper row3">
  <main class="container clear"> 

    <!-- main body -->
    <blockquote><div class="heading">Results</div>
    <?php 
      $om = $result->obtainedmark; 
      $totalMark = $set->total_mark;
    	
      if($om >= (60*$totalMark)/100)
    	$ans_col = "green";

    	elseif($om >= (40*$totalMark)/100) 
    		$ans_col= "orange";

    	else
    		$ans_col = "red";
    ?>
    <label class="fl_right {{$ans_col}}">Score: {{$om}}</label>
    
    <div>Subject: <b><i>{{$subject->name}}</i></b></div>
    <footer>Set No: {{$set->number}}</footer>
  </blockquote> <hr>
    <form >
        <ol>
            @foreach($questions as $question)
              @if($question->questiontype_id != 1)
                <strong><li class="question ">{{$question->question}}</li><div class="badge" style="float: right;background-color: #a2b70d;">Weightage: {{$question->weightage}}</div></li></strong>
              @endif
              @if($question->questiontype_id == 3)
                <?php
                $i =1; 
                ?>
                    @foreach($question->answers as $answer)
                      <div class="checkbox disabled"><label class="
                                    @if(array_key_exists($answer->id, $performance))
                                      @if( $performance[$answer->id]== true) rightans
                                      @else wrongans
                                      @endif
                                    @endif
                       ">
                      	<input value="{{$i++}}" type="checkbox" name="{{$question->id}}" disabled>{{$answer->answer}}
                      </label></div>
                    @endforeach
                  <hr>
                @elseif($question->questiontype_id == 4)
                  @if($currentResults["matchTheFollowing"][$question->id]["result"])
                    <div class="right-match">You did it correctly.</div>
                  @else
                    <div class="wrong-match">You did it wrong.</div>
                  @endif
                @endif
             @endforeach
        </ol>
      {{csrf_field()}}
        <input type="hidden" value="{{$subject->id}}"  name="subject_id">
        <input type="hidden" value="{{$set->id}}"   name="set_id">
        <input type="hidden" value="1"    name="user_id">
    </form>
    </div>
<style type="text/css">
	.rightans{
		color:green;
	}
	.wrongans{
		color:red;
	}
</style>