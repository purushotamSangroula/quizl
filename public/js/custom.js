$(document).ready(function(){

	var currentlyVisible = 0;
	//sticky header js
	 $(window).scroll(function () {
    // sticky header
    if ($(this).scrollTop() >= $('header').height() ) {
    $('body').addClass('mysticky');
    } else {
    $('body').removeClass('mysticky');
    }
    });


	$("#question_type_select").on("change", function(){
		var selectedOption = $("#question_type_select").val();
		if(selectedOption == 3)
		{
			if($("#match-the-following-tbody").length){
				document.getElementById("match-the-following-tbody").innerHTML="";
			}
			$("#match-the-following,#question-with-subquestions").hide();
			$("#mcq_question,.no-of-options").show();
		}
		else if(selectedOption == 4)
		{
			if($("#answer_options").length){
				document.getElementById("answer_options").innerHTML = ""; // get tag element
			}
			if($("#correct_answers").length){
				document.getElementById("correct_answers").innerHTML = ""; // 
			}
			$("#mcq_question,#question-with-subquestions").hide();
			$("#match-the-following,.no-of-options").show();
		}
		else if(selectedOption == 5)
		{
			if($("#answer_options").length){
				document.getElementById("answer_options").innerHTML = ""; // get tag element
			}
			if($("#correct_answers").length){
				document.getElementById("correct_answers").innerHTML = ""; // 
			}
			$("#mcq_question,#match-the-following,.no-of-options").hide();
			$("#question-with-subquestions").show();
		}
	});
	/*timer*/
	$("#Start").on("click", function(){
		startTimer();
	});
	function startTimer(){
		var timeDuration = 120;
		if(typeof timeForSet != 'undefined'){
			var timeDuration = timeForSet;
		}
		target_date = new Date().getTime() + (1000*timeDuration*60); // set the countdown date
		var days, hours, minutes, seconds; // variables for time units

		var countdown = document.getElementById("tiles"); // get tag element
		if(countdown){
			getCountdown();

			setInterval(function () { getCountdown(); }, 1000);
		}
		function getCountdown(){

		  // find the amount of "seconds" between now and target
		  var current_date = new Date().getTime();
		  var seconds_left = (target_date - current_date) / 1000;

		  hours = pad( parseInt(seconds_left / 3600) );
		  seconds_left = seconds_left % 3600;
		      
		  minutes = pad( parseInt(seconds_left / 60) );
		  seconds = pad( parseInt( seconds_left % 60 ) );
		  if( hours > -1 &&  minutes > -1 &&  seconds > -1 ){
			  countdown.innerHTML = "<span id='hour_timer'>" + hours + "</span><span id='minute_timer'>" + minutes + "</span><span id='second_timer'>" + seconds + "</span>"; 
		  }
		  if( hours == 0 && minutes == 0 && seconds == 0)
		  {
		  	console.log('ooooo');
		  	$(".out-of-time").html("TIME OVER");
		  	$('#exam-form input[type=checkbox]').attr('disabled','true');
			$("#navigation-prev").hide();
			$("#exam-submit").show();
			$("#navigation-next").hide();
			$(".draggable").attr('draggable' , 'false');
			$(".selectable-question").attr('disabled' , 'true');
		  }
		}
		function pad(n) {
		  return (n < 10 ? '0' : '') + n;
		}
	}
	/*timer*/

	/*options*/
	addOptions();
	function addOptions(){

		var answer_options = document.getElementById("answer_options"); // get tag element
		var correct_answers = document.getElementById("correct_answers"); // get tag element
		var matchTheFollowingDiv = document.getElementById("match-the-following-tbody");
		var questionWithSubQuestion = document.getElementById("sub-questions");

		var lengthOfChildren = $("#answer_options").children().length;
		var lengthOfMOFChildren = $("#match-the-following-tbody").children().length;
		var lengthOfQWSQChildren = $("#sub-questions").children().length;

		var text  ='';
		var answers  ='';
		var question_type_select = $('#question_type_select').val();
		if(answer_options || matchTheFollowingDiv || questionWithSubQuestion){
			if(question_type_select == 3)
			{
				if(matchTheFollowingDiv){
					matchTheFollowingDiv.innerHTML = "";
				}
				$("#mcq_question").show();
				$("#match-the-following").hide();
				var text  = answer_options.innerHTML;
				var answers  = correct_answers.innerHTML;

				var numberOfOptions = $("#noofoptions").val();
				if(numberOfOptions > lengthOfChildren)
				{
					
					for ( i = lengthOfChildren+1; i <= numberOfOptions; i++) { 
					    text += "<li id=ans"+i+"' ><label><input type='text' name='ans"+i+"'></label></li>";
					    answers += '<li><input type="checkbox" name="correct_answers[]" value="'+i+'">'+i+'</li>';
					}
					answer_options.innerHTML = text;
					correct_answers.innerHTML = answers;
				}
				else if(numberOfOptions < lengthOfChildren)
				{
					var N = numberOfOptions;
					$("#answer_options").children().slice(N).detach();
					$("#correct_answers").children().slice(N).detach();

				}
			}
			else if(question_type_select == 4 )
			{
				if(answer_options){
					answer_options.innerHTML = '';
					correct_answers.innerHTML = '';
				}
				var text  = matchTheFollowingDiv.innerHTML;

				var numberOfOptions = $("#noofoptions").val();
				var selectOptions = '';
				for ( i = 1; i <= numberOfOptions; i++) {
					selectOptions += "<option value= "+i+">"+i+"</option>";
				}
				if(numberOfOptions > lengthOfMOFChildren)
				{
					for ( i = lengthOfMOFChildren+1; i <= numberOfOptions; i++) { 
					    text += "<tr>"+
					    			"<td><textarea name='subquestions[]' class='form-control' type='text' name=''></textarea></td>"+
					    			"<td><select name='correct_answers[]' class='btn dropdown-toggle'>"+
					    			selectOptions+
					    			"</select></td>"+
                                    "<td><span class='serial_number'>"+i+"</span></td>"+
                                    "<td><textarea class='form-control' type='text' name='subanswers[]'></textarea></td>"+
                                "</tr>";
					}
					matchTheFollowingDiv.innerHTML = text;
				}
				else if(numberOfOptions < lengthOfMOFChildren)
				{
					var N = numberOfOptions;
					$("#match-the-following-tbody").children().slice(N).detach();
				}
				var selectedOptionsBackup = [];
				$tableRows = $("#match-the-following-tbody").children();
				$tableRows.each(function( index ) {
				  selectedOptionsBackup.push($(this).children().children("select").val() );
				  $(this).children().children("select").html( selectOptions );
				});
				$tableRows.each(function( index ) {
					if(index< selectedOptionsBackup.length ){
					  $(this).children().children("select").val(selectedOptionsBackup[index]) ;
					}
				});
			}
			else if(question_type_select == 5 )
			{
				console.log('pururpuru');
				$("#mcq_question,#match-the-following,.no-of-options").hide();
				if(answer_options){
					answer_options.innerHTML = '';
					correct_answers.innerHTML = '';
				}
				var text  = questionWithSubQuestion.innerHTML;

				var numberOfOptions = $("#numofsubq").val();
				if(numberOfOptions > lengthOfQWSQChildren)
				{
					console.log('lskjdf');
					for ( i = lengthOfQWSQChildren+1; i <= numberOfOptions; i++) {
					    text += '<div class="sub-question" style="border:10px solid darkgrey; margin-bottom:10px; ">'+
                                            '<div class="form-group">'+
                                              '<label class="control-label col-lg-4">Sub Question</label>'+
                                                '<textarea name="subquestions['+i+'][]" style="width:60%;"></textarea>'+
                                            '</div>'+
                                            '<hr>'+
                                            '<div class="form-group">'+
                                              '<label class="control-label col-lg-4">No of Answers</label>'+
                                                '<input type="number" class="number-of-answers" name="number-of-answers['+i+']">'+
                                            '</div>'+
                                            '<div class="form-group">'+
                                              '<label class="control-label col-lg-4">Answers</label>'+
                                                '<label>'+
                                                    '<ol class="ol-answers">'+
                                                    '</ol> '+
                                                  '</label>'+
                                            '</div>'+
                                            '<hr>'+
                                            '<div class="form-group correct-answer">'+
                                                '<label class="control-label col-lg-4">Correct Answer</label>'+
                                                    '<label>'+
                                                        '<select name= correct-answer['+i+'] ></select>'+
                                                    '</label>'+
                                                '</div><hr>'+
                                        '</div>';
					}
					questionWithSubQuestion.innerHTML = text;
				}
				else if(numberOfOptions < lengthOfQWSQChildren)
				{
					var N = numberOfOptions;
					$("#sub-questions").children().slice(N).detach();
				}
			}
		}
	}
	
	$("#noofoptions,#numofsubq").focusout( function(){
		addOptions();
	});
	$("#sub-questions").on("focusout", ".number-of-answers", function(){
		var noOfDropdowns = $(this).val();
		var answersInDropdownsDiv = $(this).closest(".form-group").next(".form-group").find("ol");
		var noOfChildren = answersInDropdownsDiv.children().length;
		var oldChildren = answersInDropdownsDiv.html();
		var text = oldChildren;
		var containingSubquestionDiv = $(this).closest(".sub-question");
		var question_no = $(this).closest("#sub-questions").children(".sub-question").index(containingSubquestionDiv ) +1;
		console.log("question_no" , question_no);
		if(noOfDropdowns > noOfChildren)
		{
			console.log('lskjdf');
			for ( i = noOfChildren+1; i <= noOfDropdowns; i++) {
			    text += '<li><input type="text" name="answers['+question_no+'][]"></li>';
			}
			answersInDropdownsDiv.html(text);
		}
		else if(noOfDropdowns < noOfChildren)
		{
			var N = noOfDropdowns;
			answersInDropdownsDiv.children().slice(N).detach();
		}
		var selectOptions = '';
		var correctAnswersDropdowns = $(this).closest(".form-group").siblings(".correct-answer").find("select");
		for ( i = 1; i <= noOfDropdowns; i++) {
			selectOptions += "<option value= "+i+">"+i+"</option>";
		}
		correctAnswersDropdowns.html(selectOptions);
	});
	/*options*/

	/*limit the answer selection*/
	$(':checkbox').change(function() {
		var numberOfAnswers  = $(this).closest('div.one_question').find('.numberOfAnswers').val();
		var checked = $(this).is(':checked');
		var noOfCheckedSiblings = $(this).closest('div.one_question').find(":checkbox:checked").length; 
		console.log(noOfCheckedSiblings);
		if(noOfCheckedSiblings >= numberOfAnswers)
		{
			$(this).closest('div.one_question').find(":checkbox").attr('disabled', true);
			var checkedElements = $(this).closest('div.one_question').find(":checkbox:checked");
			checkedElements.each( function( index ) {
				  $(this).attr('disabled', false);
			});
		}
		else
		{
			var allCheckboxElements = $(this).closest('div.one_question').find(":checkbox");
			allCheckboxElements.each( function( index ) {
				  $(this).attr('disabled', false);
			});
		}
	}); 
	/*limit the answer selection*/
	$("form#set_quiz").on('submit' , function(){
		if(!confirm('Are you sure you want to submit? Some answers might be wrong or unanswered.') )
        return false;
	});
	
	/*DRAGGABLE*/
	// var btn = document.querySelector('.add');
	var remove = document.querySelector('.draggable');

	function dragStart(e) {
	  this.style.opacity = '0.4';
	  dragSrcEl = this;
	  e.dataTransfer.effectAllowed = 'move';
	  e.dataTransfer.setData('text/html', this.innerHTML);
	};

	function dragEnter(e) {
	  this.classList.add('over');
	}

	function dragLeave(e) {
	  e.stopPropagation();
	  this.classList.remove('over');
	}

	function dragOver(e) {
	  e.preventDefault();
	  e.dataTransfer.dropEffect = 'move';
	  return false;
	}

	function dragDrop(e) {
	  if (dragSrcEl != this) {
	    dragSrcEl.innerHTML = this.innerHTML;
	    this.innerHTML = e.dataTransfer.getData('text/html');
	  }
	  return false;
	}

	function dragEnd(e) {
	  var listItens = document.querySelectorAll('.draggable');
	  [].forEach.call(listItens, function(item) {
	    item.classList.remove('over');
	  });
	  this.style.opacity = '1';
	}

	function addEventsDragAndDrop(el) {
	  el.addEventListener('dragstart', dragStart, false);
	  el.addEventListener('dragenter', dragEnter, false);
	  el.addEventListener('dragover', dragOver, false);
	  el.addEventListener('dragleave', dragLeave, false);
	  el.addEventListener('drop', dragDrop, false);
	  el.addEventListener('dragend', dragEnd, false);
	}

	var listItens = document.querySelectorAll('.draggable');
	[].forEach.call(listItens, function(item) {
	  addEventsDragAndDrop(item);
	});
	/*DRAGGABLE*/
	function arrayUnique(array) {
	    var a = array.concat();
	    for(var i=0; i<a.length; ++i) {
	        for(var j=i+1; j<a.length; ++j) {
	            if(a[i] === a[j])
	                a.splice(j--, 1);
	        }
	    }

	    return a;
	}
	var currentlyVisible = 0;
	/* FLAG THE INCOMPLETE QUESTIONS.*/
	$(".unanswered-questions").hide();
	var unansweredQuestions = new Array();
	var flaggedQuestions = new Array();

	function individualFlagAtBottom(questionIndex)
	{
		console.log(questionIndex ,  "in" , flaggedQuestions);
		if(flaggedQuestions.includes(questionIndex))
		{
			$("#flag-button").addClass("flagged");
		}
		else
		{
			// console.log("remove"+questionIndex);
			$("#flag-button").removeClass("flagged");
		}
	}
	function flagFlaggedQuestion(questionIndex)
	{
		if(!flaggedQuestions.includes(questionIndex))
		{
			flaggedQuestions.push(questionIndex)
			individualFlagAtBottom(questionIndex);
		}
		else
		{
			var index = flaggedQuestions.indexOf(questionIndex);
			if (index > -1) {
			  flaggedQuestions.splice(index, 1);
			}
			individualFlagAtBottom(questionIndex);
		}
	}
	$("#flag-button").click(function(){
		flagFlaggedQuestion(currentlyVisible);
	});
	/*
	* next 1, prev 2
	*/
	function showFlaggedOrUnansweredQuestions(nextOrPrev)
	{
		var tempUas = "";
		mergedQuestions = arrayUnique( unansweredQuestions.concat(flaggedQuestions) );
		mergedQuestions.forEach(function(data){
			var check = data + 1 ;
			if(flaggedQuestions.includes( data ))
			{
				tempUas+= "<span class= flagged><i class='fa fa-flag'></i>"+check+"</span>";
			}
			else
			{
				tempUas+= "<span>"+check+"</span>";				
			}
		});
		$("#unanswered-questions").html(tempUas);
		if( $(".unanswered-questions").length > 0 )
		{
			$(".unanswered-questions").show();
		}
		else
		{
			$(".unanswered-questions").hide();
		}
	}
	function flagIncompleteQuestion(questionIndex )
	{
		var question = $("#question_answers").children(".one_question").eq(questionIndex);
		if( question.find(".numberOfAnswers").length )
		{
			var numberOfAnswers = question.find(".numberOfAnswers").first().val();
			var selectedNumberOfAnswers = question.find(":checkbox:checked").length; 
			if(selectedNumberOfAnswers < numberOfAnswers)
			{
				if(!unansweredQuestions.includes(questionIndex))
				{
					unansweredQuestions.push(questionIndex);
				}
			}
			else if(selectedNumberOfAnswers == numberOfAnswers)
			{
				if(unansweredQuestions.includes(questionIndex))
				{
					var index = unansweredQuestions.indexOf(questionIndex);
					if (index > -1) {
					  unansweredQuestions.splice(index, 1);
					}
				}
			}
		}
		showFlaggedOrUnansweredQuestions(1);
	}
	/*STEP BY STEP*/
	$("#question_answers").children(".one_question").hide();
	$("#question_answers").children(".one_question").eq(currentlyVisible).show();	
	$("#navigation-prev").hide();
	if(totalQuestions ==1 )
	{
		console.log('totalQuestions' , totalQuestions);
		$("#exam-submit").show();
		$("#navigation-next").hide();
	}
	else
	{
		$("#navigation-next").show();
		$("#exam-submit").hide();
	}
	$("#navigation-next").click(function(){
		flagIncompleteQuestion(currentlyVisible );
		$("#exam-submit").hide();
		if(currentlyVisible  < totalQuestions  ) 
		{
			var nextVisible = currentlyVisible + 1;
			$("#question_answers").children(".one_question").hide();
			$("#question_answers").children(".one_question").eq(nextVisible).show();
			$("#navigation-prev").show();
			if(currentlyVisible  == totalQuestions -2 ){
				$("#navigation-next").hide();
				$("#exam-submit").show();
			}
		}	
		else
		{
			$("#navigation-prev").hide();
		}
		currentlyVisible = nextVisible;
		individualFlagAtBottom(currentlyVisible);

	});
	$("#navigation-prev").click(function(){
		$("#exam-submit").hide();
		currentlyVisible-=1;
		if(currentlyVisible>=0) 
		{
			$("#question_answers").children(".one_question").hide();
			$("#question_answers").children(".one_question").eq(currentlyVisible).show();
			$("#navigation-next").show();
			if(currentlyVisible  == 0 ){
				$("#navigation-prev").hide();
			}
		}
		else
		{
			$("#navigation-next").hide();
		}
		individualFlagAtBottom(currentlyVisible);
		showFlaggedOrUnansweredQuestions(2);
	});

	/*STEP BY STEP*/
	/*go to unanswerre question*/
	$("#unanswered-questions").on("click", "span", function(){
		var clickedValue = $(this).html();
		var clickedElementIndex = parseInt(clickedValue) -1;
		$("#question_answers").children(".one_question").hide();
		$("#question_answers").children(".one_question").eq(clickedElementIndex).show();
		currentlyVisible = clickedElementIndex;
		if(currentlyVisible != totalQuestions -1  )
		{
			$("#exam-submit").hide();
			if( currentlyVisible == 0 )
			{
				$("#navigation-prev").hide();
				$("#navigation-next").show();
			}
			else
			{
				$("#navigation-prev").show();
				$("#navigation-next").show();
			}
		}

	});
	/*go to unanswerre question*/

});

