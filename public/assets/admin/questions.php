﻿<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Responsive Bootstrap Advance Admin Template</title>

    <!-- BOOTSTRAP STYLES-->
    <link href="assets/css/bootstrap.css" rel="stylesheet" />
    <!-- FONTAWESOME STYLES-->
    <link href="assets/css/font-awesome.css" rel="stylesheet" />
    <!--CUSTOM BASIC STYLES-->
    <link href="assets/css/basic.css" rel="stylesheet" />
    <!--CUSTOM MAIN STYLES-->
    <link href="assets/css/custom.css" rel="stylesheet" />
    <!-- GOOGLE FONTS-->
    <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css' />
</head>
<body>
    <div id="wrapper">
        <nav class="navbar navbar-default navbar-cls-top " role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".sidebar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="index.php">OES</a>
            </div>

            <div class="header-right">

               <a href="login.php" class="btn btn-danger" title="Logout"><i class="fa fa-exclamation-circle fa-2x">  LogOut</i></a>


            </div>
        </nav>
        <!-- /. NAV TOP  -->
        <nav class="navbar-default navbar-side" role="navigation">
            <div class="sidebar-collapse">
                <ul class="nav" id="main-menu">
                    <li>
                        <div class="user-img-div">
                            <img src="assets/img/user.png" class="img-thumbnail" />

                            <div class="inner-text">
                                Jhon Deo Alex
                            <br />
                                <small>Last Login : 2 Weeks Ago </small>
                            </div>
                        </div>

                    </li>


                    <li>
                        <a href="index.php"><i class="fa fa-dashboard "></i>Dashboard</a>
                    </li>
                   
                     <li>
                        <a href="#"><i class="glyphicon glyphicon-plus"></i>Add <span class="fa arrow"></span></a>
                         <ul class="nav nav-second-level collapse in">
                           
                             <li>
                                <a href="subjects.php"><i class="glyphicon glyphicon-book"></i>Subject </a>
                            </li>
                             <li>
                                <a href="sets.php"><i class="glyphicon glyphicon-folder-open"></i>Sets</a>
                            </li>
                             <li>
                                <a class="active-menu" href="questions.php"><i class="glyphicon glyphicon-question-sign"></i>Questions</a>
                            </li>
                        </ul>
                            
                      
                           
                        </ul>
                    </li>
                    
                </ul>
            </div>

        </nav>
        <!-- /. NAV SIDE  -->
        <div id="page-wrapper">
            <div id="page-inner">
                <div class="row">
                    <div class="col-md-12">
                        <h1 class="page-head-line">Questions</h1><div style="float: right;">
                        
                        </div>
                        <h1 class="page-subhead-line">This is the list of questions, you can add, edit or delete.</h1>

                    </div>
                </div>
                <!-- /. ROW  -->
                <div class="row">
                    <div class="col-md-6 col-sm-6 col-xs-12">
               <div class="panel panel-info">
                        <div class="panel-heading">
                           Question List
                        </div>
                        <div class="panel-body">
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered table-hover">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                           
                                            <th>Questions</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>1</td>
                                            
                                            <td>what is the meaning of the meaning when it has no meaning?</td>
                                            <td> <button class="btn btn-primary"><i class="glyphicon glyphicon-edit"></i>Edit</button>
                            <button class="btn btn-danger"><i class="glyphicon glyphicon-home"></i>Delete</button></td>
                                        </tr>
                                        <tr>
                                            <td>2</td>
                                           
                                            <td>what is the meaning of the meaning when it has no meaning?</td>
                                            <td> <button class="btn btn-primary"><i class="glyphicon glyphicon-edit"></i>Edit</button>
                            <button class="btn btn-danger"><i class="glyphicon glyphicon-home"></i>Delete</button></td>
                                        </tr>
                                        <tr>
                                            <td>3</td>
                                            
                                            <td>@twitter</td>
                                            <td> <button class="btn btn-primary"><i class="glyphicon glyphicon-edit"></i>Edit</button>
                            <button class="btn btn-danger"><i class="glyphicon glyphicon-home"></i>Delete</button></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            </div>
                        </div>
                            </div>
                    <div class="col-md-6">
                       <div class="panel panel-default">
                        <div class="panel-heading">
                           Add Questions
                        </div>
                        <div class="panel-body">
                   
                    <div class="form-group">
                        <label class="control-label col-lg-4">Question</label>
                        <textarea name="Question" style="width:65%; float: right;"></textarea>
                    </div>


                    
                                                 
    <br><br><hr>
    
    



                            
                         
                        
                    </div>
                    <div class="form-group">
                        <label class="control-label col-lg-4">Answers</label>
                       <label>
                        <ol>
                          <li>
                            <label><input type="text" name="Ans1"></li></label>
                          
                          <li>
                           <label><input type="text" name="Ans2"></li></label>
                          
                          <li>
                           <label> <input type="text" name="Ans3"></li></label>
                          
                          <li>
                            <label><input type="text" name="Ans4"></li></label>
                          
                        </ol> 
                      </label>
                    </div>
                    
                    <label><a href="#"> <button class="btn btn-inverse"><i class="glyphicon glyphicon-plus"></i>Add</button></a></label>
                    
                   </div> 
              </div>

            </div>
            <!-- /. PAGE INNER  -->
        </div>
        <!-- /. PAGE WRAPPER  -->
    </div>

    <!-- /. WRAPPER  -->
    <div id="footer-sec">
        &copy; 2014 YourCompany | Design By : <a href="http://www.binarytheme.com/" target="_blank">BinaryTheme.com</a>
    </div>
    <!-- /. FOOTER  -->

    <!-- SCRIPTS -AT THE BOTOM TO REDUCE THE LOAD TIME-->
    <!-- JQUERY SCRIPTS -->
    <script src="assets/js/jquery-1.10.2.js"></script>
    <!-- BOOTSTRAP SCRIPTS -->
    <script src="assets/js/bootstrap.js"></script>
    <!-- PAGE LEVEL SCRIPTS -->
    <script src="assets/js/bootstrap-fileupload.js"></script>
    <!-- METISMENU SCRIPTS -->
    <script src="assets/js/jquery.metisMenu.js"></script>
    <!-- CUSTOM SCRIPTS -->
    <script src="assets/js/custom.js"></script>


</body>
</html>
