<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Auth::routes();
Route::get('/' , 'HomeController@index')->name('mainHomePage');

// Route::prefix('admin')->group(function () {
// 	Route::get('login' , 'admin\AdminController@login')->name('admin.login');
// 	Route::resource('/','admin\AdminController');
// 	Route::resource('subjects','admin\SubjectsController');
// });
Route::group(['prefix' => 'admin', 'as' => 'admin.'], function()
{
 	Route::get('login' , 'admin\AdminController@login')->name('admin.login');
	Route::resource('/','admin\AdminController');
	Route::resource('subjects','admin\SubjectsController');
	Route::resource('sets','admin\SetsController');
	Route::resource('questions','admin\QuestionsController');
});
Route::resource('subjects','SubjectsController');
Route::resource('sets','SetsController');
Route::resource('results','ResultsController');
Route::resource('users','UsersController');


Route::get('results/print/{id}' , 'ResultsController@print')->name('result.print');
Route::get('logout', '\App\Http\Controllers\Auth\LoginController@logout');