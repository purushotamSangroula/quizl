<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateQuestionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('questions', function (Blueprint $table) {
            $table->increments('id');
            $table->string('question');
            $table->integer('set_id')->unsigned();
            $table->integer('questiontype_id')->unsigned();
            $table->integer('weightage');
            $table->timestamps();
        });
        Schema::table('questions', function($table){
            $table->foreign('questiontype_id')->references('id')->on('questiontypes')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('set_id')->references('id')->on('sets')->on('questiontypes')->onDelete('cascade')->onUpdate('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('questions');
    }
}
