<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SelfReferenceQuestion extends Model
{
    protected $table = 'self_reference_questions';
    protected $fillable = [
        'question_id'	,'parent_question_id'
    ];
}
