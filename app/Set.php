<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Set extends Model
{
    protected $fillable = [
        'number'	,'type',	'noofquestions'	,'subject_id',	'total_mark',	'pass_mark' , 'time'
    ];
    public function subject(){
        return $this->belongsTo('App\Subject');
    }
    public function questions(){
        return $this->hasMany('App\Question');
    }
}
