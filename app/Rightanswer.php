<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Rightanswer extends Model
{
	protected $fillable = [
        'answer_id'	,'question_id' , 'dropdown_number'
    ];
    public function question(){
        return $this->belongsTo('App\Question');
    }
    public function answer(){
        return $this->belongsTo('App\Answer');
    }

}
