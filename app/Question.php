<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Question;
use App\SelfReferenceQuestion;
class Question extends Model
{
    //
    protected $fillable = [
        'question'	,'set_id'	,'questiontype_id',	'weightage' ,'correct_option' , 'noofanswers' , 'noofoptions'
    ];
    public function set(){
        return $this->belongsTo('App\Set');
    }
    public function answers(){
        return $this->hasMany('App\Answer');
    }
    public function rightanswers(){
        return $this->hasMany('App\Rightanswer');
    }
    function getChildrenQuestions($questionId)
    {
        $childQuestions = SelfReferenceQuestion::where("parent_question_id" , "=" , $questionId )->pluck( "question_id" )->all();
        return Question::whereIn( 'id' , $childQuestions)->get();
    }
}
