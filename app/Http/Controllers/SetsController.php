<?php

namespace App\Http\Controllers;

use App\Set;
use App\Questiontype;
use App\Subject;
use App\Question;
use Illuminate\Support\Facades\Auth;

use Illuminate\Http\Request;

class SetsController extends Controller
{
    public function show(Set $set)
    {
    	if(Auth::check())
    	{
	        $questions = Set::find($set->id)->questions;
	        $question_types = Questiontype::all();
	        $totalQuestions = Question::where('set_id' , $set->id)->whereIn('questiontype_id' , [4, 3, 5])->get();
	        $subject = Subject::where('id' , '=' , $set->subject_id)->first();
	        return view('frontend.sets.show' , ['set'=> $set , 'questions'=>$questions , 'subject'=>$subject , 'question_types'=>$question_types, 'totalQuestions'=> count($totalQuestions) ]);
	    }
	    return redirect()->route("login");
    }
}
