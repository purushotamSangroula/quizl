<?php

namespace App\Http\Controllers;

use App\Result;
use App\Question;
use App\Set;
use Illuminate\Http\Request;
use App\Questiontype;
use App\Subject;
use App\SelfReferenceQuestion;
use App\Rightanswer;
use App\Answer;

use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Auth;

class ResultsController extends Controller
{
    const SubQuestions = 1;
    const MCQ = 3;
    const MatchTheFollowing = 4;
    const QWSQ = 5;


    public function print(Request $request,$id  )
    {
        $currentResults = $request->session()->get('results');
        $result = Result::find($id);
        $questions = Set::find($result->set_id)->questions;
        $set = Set::find($result->set_id);
        $question_types = Questiontype::all();
        $subject = Subject::where('id' , '=' , $result->subject_id)->first();
        return view('frontend.results.print' , ['set'=> $set , 'questions'=>$questions , 'subject'=>$subject , 'question_types'=>$question_types ,'performance'=>$currentResults["results"] , 'result'=> $result, 'currentResults'=>$currentResults]);
    }
    public function checkMatchTheFollowing($question , $submittedAnswers)
    {
        $childRightAnswers = $this->getChildrenRightAnswers($question->id);
        $result = true;
        for($i = 0 ; $i < count($submittedAnswers); $i++)
        {
            if($submittedAnswers[$i] != $childRightAnswers[$i])
            {
                $result = false;
                break;
            }
        }
        return $result;
    }
    public function checkQuestionWithSubQuestions($question , $submittedAnswers)
    {
        $childRightAnswers = $this->getChildrenRightAnswers($question->id);
        $result = true;
        for($i = 0 ; $i < count($submittedAnswers); $i++)
        {
            if($submittedAnswers[$i] != $childRightAnswers[$i])
            {
                $result = false;
                break;
            }
        }
        return $result;
    }
    public function getobtainedMark($submittedAnswers, $set_id){
        $obtainedMark = 0;
        $results = [];
        $matchTheFollowing = [];             
        $printResult = [];
        $questionWithSubQuestions = [];
        // dd($submittedAnswers);
        foreach ($submittedAnswers as $questionId => $selectedAnswers) {
            $question = Question::where(['id'=>$questionId])->first();            
            if($question->questiontype_id == self::MCQ )
            {
                $rightAnswerIds = $question->rightanswers()->pluck('answer_id')->all();
                $allAnswerRight = true;

                foreach ($selectedAnswers as $key => $answerId) {
                    $results[$answerId] = false;
                    if(!in_array($answerId, $rightAnswerIds)){
                        $allAnswerRight = false;
                    }
                    else
                    {
                        $results[$answerId] = true;
                    }
                }
                if( count($selectedAnswers) < count($rightAnswerIds) ) $allAnswerRight = false;
                $printResult[$question->id] = false;
                if($allAnswerRight)
                {
                    $obtainedMark += $question->weightage;
                    $printResult[$question->id] = true;
                }
            }
            elseif ($question->questiontype_id == self::MatchTheFollowing ) 
            {
                $matchTheFollowingResult = $this->checkMatchTheFollowing($question , $selectedAnswers);
                $printResult[$question->id] = false;
                if($matchTheFollowingResult)
                {
                    $obtainedMark += $question->weightage;
                    $printResult[$question->id] = true;
                }
                $matchTheFollowing[$question->id] = [ "result" => $matchTheFollowingResult , "submittedAnswers"=>$selectedAnswers , "rightAnswers" => $this->getChildrenRightAnswers($question->id) ];
            }
            elseif ($question->questiontype_id == self::QWSQ ) 
            {
                $questionWithSubQuestionResult = $this->checkQuestionWithSubQuestions($question , $selectedAnswers);
                $printResult[$question->id] = false;
                if($questionWithSubQuestionResult)
                {
                    $obtainedMark += $question->weightage;
                    $printResult[$question->id] = true;
                }
                $questionWithSubQuestions[$question->id] = [ "result" => $questionWithSubQuestionResult , "submittedAnswers"=>$selectedAnswers , "rightAnswers" => $this->getChildrenRightAnswers($question->id) ];
            }

        }
        $questions = Set::find($set_id)->questions;
        /*make green the right answers*/
        foreach ($questions as $key => $question) {
            $rightanswers = $question->rightanswers;
            foreach ($rightanswers as $key => $answer) {
                $results[$answer->answer_id] = true;
            }
            if( $question->questiontype_id ==4 ||  $question->questiontype_id == 3)
            {
                if( !array_key_exists($question->id, $printResult)) $printResult[$question->id] = false; 
            }
        }
        // dd($results);
        return $allResults = ['obtainedmark'=>$obtainedMark , 'results'=>$results , 'matchTheFollowing'=> $matchTheFollowing , 'questionWithSubQuestions'=> $questionWithSubQuestions , 'printResult'=> $printResult];
    }
    public function store(Request $request)
    {
        $user_id = 7;
        if(Auth::check())
        {
            $user_id = Auth::user()->id;
        }
        $data = $request->except('_token');
        // dd($data);
        $submittedAnswers = $request->except(['_token' , 'subject_id','set_id', 'user_id']);
        $results = $this->getobtainedMark($submittedAnswers , $request->set_id);
        $om = $results["obtainedmark"];
        if($om>=80) $res = "Excellent";
        else if($om>=60) $res= "Good";
        else if($om>=50) $res="Okay";
        else $res= "Not good"; 
        try{
            $result = Result::create([
                "user_id"=> $user_id,
                "subject_id" => $request->subject_id,
                "set_id"=>$request->set_id,
                "obtainedmark"=>$results["obtainedmark"],
                "result"=>$res
            ]);
        }
        catch(\Exception $e){
            // dd($e);
            return back()->withInput()->with('error','failed to submit');
        }
        session(['results' => $results]);
        return redirect()->route('results.show' , $result->id );
    }

    public function show( Request $request,$id  )
    {
        $currentResults = $request->session()->get('results');
        $result = Result::find($id);
        $questions = Set::find($result->set_id)->questions;
        $set = Set::find($result->set_id);
        $question_types = Questiontype::all();
        $subject = Subject::where('id' , '=' , $result->subject_id)->first();
        return view('frontend.results.show' , ['set'=> $set , 'questions'=>$questions , 'subject'=>$subject , 'question_types'=>$question_types ,'performance'=>$currentResults["results"] , 'result'=> $result, 'currentResults'=>$currentResults]);
    }
    function getChildrenQuestions($questionId)
    {
        $childQuestions = SelfReferenceQuestion::where("parent_question_id" , "=" , $questionId )->pluck( "question_id" )->all();
        return Question::whereIn( 'id' , $childQuestions)->get();
    }
    function getChildrenRightAnswers($questionId)
    {
        $childQuestions = SelfReferenceQuestion::where("parent_question_id" , "=" , $questionId )->pluck( "question_id" )->all();
        $childRightAnswers = Rightanswer::whereIn("question_id"  , $childQuestions )->pluck( "answer_id" )->all();
        return $childRightAnswers; 
    }
}
