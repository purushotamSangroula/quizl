<?php

namespace App\Http\Controllers\admin;

use App\Set;
use App\Subject;
use App\Questiontype;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SetsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $questions = Set::all();
        return view('admin.questions.index' , ['questions'=>$questions]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request->except('_token'));
        try{
            Set::create($request->except('_token'));
        }
        catch(\Exception $e){
            // dd($e);
            return back()->withInput()->with('error' , 'Subjects could not be added');
        }
        return back()->with('success' , 'Subjects adding successful.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Set  $set
     * @return \Illuminate\Http\Response
     */
    public function show(Set $set)
    {
        $questions = Set::find($set->id)->questions;
        $question_types = Questiontype::all();
        // dd($question_types);
        $subject = Subject::where('id' , '=' , $set->subject_id)->first()->name;
        return view('admin.sets.show' , ['set'=> $set , 'questions'=>$questions , 'subject'=>$subject , 'question_types'=>$question_types ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Set  $set
     * @return \Illuminate\Http\Response
     */
    public function edit(Set $set)
    {
        // $subject_id = Set::find($set->id)->subject_id;
        return view('admin.sets.edit' ,['set'=> $set  ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Set  $set
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Set $set)
    {
        $set = Set::where('id' , $set->id);
        $setUpdate = $set->update($request->except(['_token' , '_method']) );
        if($setUpdate){
            return redirect()->route('admin.subjects.show', $set->first()->subject_id)->with('success', 'set updated successfully');
        }
        //redirect
        return back()->withInput();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Set  $set
     * @return \Illuminate\Http\Response
     */
    public function destroy( $id)
    {
        //
        $findSubject = Set::find( $id);
        if($findSubject->delete()){
            return redirect()->back()->with('success', 'Set deleted successfully');
        }
        return back()->withInput()->with('error' , 'Set could not be deleted');
    }
}
