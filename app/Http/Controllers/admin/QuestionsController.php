<?php

namespace App\Http\Controllers\admin;

use App\Question;
use App\Answer;
use App\Questiontype;
use App\Rightanswer;
use App\SelfReferenceQuestion;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class QuestionsController extends Controller
{
    const MTF = 4;
    const MCQ = 3;
    const QWSQ = 5;


    public function index()
    {
        //
    }
    public function create()
    {
        //
    }
    public function store(Request $request)
    {
        /* answers means options */
        $data = $request->except('_token');
        $questiontype_id = $request->questiontype_id;
        $noOfOptions = $request->noofoptions;
        if($questiontype_id == self::MCQ )
        {
            $answers = [];
            $createdAnswers = [];
            for($i = 1; $i <= $noOfOptions ; $i++)
            {
                $answers[$i] = $request["ans".$i]; 
                unset($data['ans'.$i]);
            }
            $correctAnswers = $request->correct_answers; 
            unset($data['correct_answers']);
            $noofanswers = count($correctAnswers);
            $data["noofanswers"] = $noofanswers;
            var_dump($correctAnswers);
            try{
                $question = Question::create($data);
                foreach ($answers as $key => $value) {
                    $answer = Answer::create([
                        "answer"=>  $value,
                        "question_id"=> $question->id,
                    ]);
                    $createdAnswers[] = $answer->id;

                }
                foreach ($correctAnswers as $key => $value) {
                    Rightanswer::create([
                        "answer_id"=>  $createdAnswers[$value-1],
                        "question_id"=> $question->id,
                    ]);
                }
            }
            catch(\Exception $e){
                return back()->withInput()->with('error','Question could not be added.');
            }
        }
        elseif ($questiontype_id == self::MTF ) 
        {
            // dd($data);  
            $answers = [];
            $createdAnswers = [];
            for($i = 1; $i <= $noOfOptions ; $i++)
            {
                $answers[$i] = $request["ans".$i]; 
                unset($data['ans'.$i]);
            }
            $correctAnswers = $request->correct_answers; 
            //unset($data['correct_answers']);
            $noofanswers = count($correctAnswers);
            $data["noofanswers"] = $noofanswers;
            $parentId = null;
            var_dump($correctAnswers);
            try{
                $question = Question::create($data);
                $parentId = $question->id;
            }
            catch(\Exception $e){
                return back()->withInput()->with('error','Question could not be added.');
            }
            /*subquestions and subanswers*/
            try{
                $subanswers = $data["subanswers"];
                $subquestions = $data["subquestions"];
                $createdSubAnswers = [];
                $createdSubQuestions = [];

                for ($i = 0; $i< count($subquestions) ; $i++) {
                    $data["question"] = $subquestions[$i];
                    $data["noofoptions"] = 1;
                    $data["noofanswers"] = 1;
                    $data["questiontype_id"] = 1; // Reserved types for subquestions

                    $answer = $subanswers[$i];

                    $question = Question::create($data);
                    $answer = Answer::create([
                        "answer"=>  $answer,
                        "question_id"=> $question->id,
                    ]);

                    $createdSubAnswers[] = $answer->id;
                    $createdSubQuestions[] = $question->id;
                    SelfReferenceQuestion::create([
                        "question_id" => $question->id,
                        "parent_question_id" => $parentId ,
                    ]);
                }
                for($i = 0 ; $i< count($correctAnswers); $i++ ) {
                    Rightanswer::create([
                        "answer_id"=>  $createdSubAnswers[(int)$correctAnswers[$i] -1],
                        "question_id"=> $createdSubQuestions[$i],
                        "dropdown_number" => $correctAnswers[$i],

                    ]);
                }
                //dd($data);

            }
            catch(\Exception $e){
                dd($e);
                return back()->withInput()->with('error','Question could not be added.');
            }
            /*subquestions and subanswers*/

        }
        elseif ($questiontype_id == self::QWSQ ) 
        {
            $parentQuestion = $request->question;
            $answers = $request->answers;
            $createdAnswers = [];
            $correctAnswers = $data["correct-answer"]; 
            $noOfSubQuestions = $request->numofsubq;
            //unset($data['correct_answers']);
            $noofanswers = count($correctAnswers);
            $data["noofanswers"] = $noofanswers;
            $data["noofoptions"] = $data["numofsubq"];

            $parentId = null;
            try{
                $question = Question::create($data);
                $parentId = $question->id;
            }
            catch(\Exception $e){
                dd($e);
                return back()->withInput()->with('error','Question could not be added.');
            }
            /*subquestions and subanswers*/
            try{
                $subquestions = $data["subquestions"];
                $createdSubAnswers = [];
                $createdSubQuestions = [];
                for ($i = 1; $i<= count($subquestions) ; $i++) {
                    $data["question"] = $subquestions[$i][0];
                    $data["noofoptions"] =  $data["number-of-answers"][$i];
                    $data["noofanswers"] = 1;
                    $data["questiontype_id"] = 1; // Reserved types for subquestions

                    $answersForSQ = $answers[$i];
                    $question = Question::create($data);
                    for( $j = 0 ; $j < count($answersForSQ ) ; $j++)
                    {
                        $answer = Answer::create([
                            "answer"=>  $answersForSQ[$j],
                            "question_id"=> $question->id,
                        ]);
                        $createdSubAnswers[$i][] = $answer->id;
                    }
                    $createdSubQuestions[$i] = $question->id;
                    SelfReferenceQuestion::create([
                        "question_id" => $question->id,
                        "parent_question_id" => $parentId ,
                    ]);
                }
                for($i = 1 ; $i<= count($correctAnswers); $i++ ) {
                    Rightanswer::create([
                        "answer_id"=>  $createdSubAnswers[$i][(int)$correctAnswers[$i] -1],
                        "question_id"=> $createdSubQuestions[$i],
                        "dropdown_number" => $correctAnswers[$i],

                    ]);
                }
            }
            catch(\Exception $e){
                dd($e);
                return back()->withInput()->with('error','Question could not be added.');
            }
            /*subquestions and subanswers*/

        }
        return back()->with('success','Question added successfully.');
    }
    public function show(Questions $questions)
    {
    }

    public function edit( $question)
    {
        $answers = [];
        $question_types = Questiontype::all();
        $question = Question::find($question);
        $questiontype_id = $question->questiontype_id;
        $childQuestions = [];
        $ans = null;
        $childRightAnswersDropdowns = null;
        if($questiontype_id == self::MCQ){
            $rightAnswers = $question->rightanswers;
            $plucked = $rightAnswers->pluck( 'answer_id');
            $rightAnswers = $plucked->all();
            $ans = $question->answers;
            $i=1;
            foreach ($ans as $key => $value) {
                $answers["ans".$i++]= $value->answer;
            }
        }
        elseif ($questiontype_id == self::MTF ) 
        {
            $childQuestions = $this->getChildrenQuestions( $question->id );
            $answers = $childAnswers = $this->getChildrenAnswers( $question->id );
            $rightAnswers = $childRightAnswers = $this->getChildrenRightAnswers( $question->id );
            $childRightAnswersDropdowns = $this->getChildrenRightAnswersDropdown( $question->id );
        }
        elseif ($questiontype_id == self::QWSQ ) 
        {
            $childQuestions = $this->getChildrenQuestions( $question->id );
            $answers = $childAnswers = $this->getChildrenAnswersForQWSQ( $question->id );
            $rightAnswers = $childRightAnswers = $this->getChildrenRightAnswers( $question->id );
            $childRightAnswersDropdowns = $this->getChildrenRightAnswersDropdown( $question->id );

        }
        return view('admin.questions.edit' ,["question"=> $question , 'question_types'=>$question_types , 'answers'=>$answers , 'rightAnswers'=> $rightAnswers , 'ansObject'=> $ans , "questiontype_id" => $questiontype_id , "childQuestions" => $childQuestions , "childRightAnswersDropdowns" => $childRightAnswersDropdowns ]);
    }
    public function update(Request $request,  $id)
    {
        $question = null;
        $data = $request->except(['_token' , '_method']);
        $noOfOptions = $request->noofoptions;
        $questiontype_id = $request->questiontype_id;

        if($questiontype_id == self::MCQ )
        {
            $answers = [];
            $createdAnswers = [];
            for($i = 1; $i <= $noOfOptions ; $i++)
            {
                $answers[$i] = $request["ans".$i]; 
                unset($data['ans'.$i]);
            }
            $correctAnswers = $request->correct_answers; 
            unset($data['correct_answers']);
            $noofanswers = count($correctAnswers);
            $data["noofanswers"] = $noofanswers;
            var_dump($correctAnswers);
            $this->deleteAnswers($id);
            try{
                    $question = Question::where('id' , $id);
                    $questionUpdate = $question->update($data);
                    foreach ($answers as $key => $value) {
                        $answer = Answer::create([
                            "answer"=>  $value,
                            "question_id"=> $id,
                        ]);
                        $createdAnswers[] = $answer->id;

                    }
                     var_dump($createdAnswers);
                    foreach ($correctAnswers as $key => $value) {
                        Rightanswer::create([
                            "answer_id"=>  $createdAnswers[$value-1],
                            "question_id"=> $id,
                        ]);
                    }
                }
            catch(\Exception $e){
                dd($e);
                return back()->withInput()->with('error','Question could not be added.');
            }
        }
        elseif ($questiontype_id == self::MTF ) 
        {
            // dd($data);  
            $answers = [];
            $createdAnswers = [];
            for($i = 1; $i <= $noOfOptions ; $i++)
            {
                $answers[$i] = $request["ans".$i]; 
                unset($data['ans'.$i]);
            }
            $correctAnswers = $request->correct_answers; 
            //unset($data['correct_answers']);
            $noofanswers = count($correctAnswers);
            $data["noofanswers"] = $noofanswers;
            $parentId = null;
            $subanswers = $data["subanswers"];
            $subquestions = $data["subquestions"];
            unset($data["subanswers"]);
            unset($data["subquestions"]);
            unset($data["correct_answers"]);

            $this->deleteAnswers($id);
            try{
                $question = Question::where('id' , $id);
                $questionUpdate = $question->update($data);
                $parentId = $id;
                $parentSetId = $question->get()->first()->set_id;
            }
            catch(\Exception $e){
                dd($e);
                return back()->withInput()->with('error','Question could not be added.');
            }
            /*subquestions and subanswers*/
            try{
                $createdSubAnswers = [];
                $createdSubQuestions = [];

                for ($i = 0; $i< count($subquestions) ; $i++) {
                    $data["question"] = $subquestions[$i];
                    $data["noofoptions"] = 1;
                    $data["noofanswers"] = 1;
                    $data["questiontype_id"] = 1; // Reserved types for subquestions
                    $data["set_id"] = $parentSetId;

                    $answer = $subanswers[$i];

                    $question = Question::create($data);
                    $answer = Answer::create([
                        "answer"=>  $answer,
                        "question_id"=> $question->id,
                    ]);

                    $createdSubAnswers[] = $answer->id;
                    $createdSubQuestions[] = $question->id;
                    SelfReferenceQuestion::create([
                        "question_id" => $question->id,
                        "parent_question_id" => $parentId ,
                    ]);
                }
                for($i = 0 ; $i< count($correctAnswers); $i++ ) {
                    Rightanswer::create([
                        "answer_id"=>  $createdSubAnswers[(int)$correctAnswers[$i] -1],
                        "question_id"=> $createdSubQuestions[$i],
                        "dropdown_number" => $correctAnswers[$i],

                    ]);
                }
                //dd($data);

            }
            catch(\Exception $e){
                dd($e);
                return back()->withInput()->with('error','Question could not be added.');
            }
            /*subquestions and subanswers*/
            return redirect()->route('admin.sets.show', $parentSetId)->with('success','Question added successfully.');

        }
        elseif ($questiontype_id == self::QWSQ ) 
        {
            $parentQuestion = $request->question;
            $answers = $request->answers;
            $createdAnswers = [];
            $correctAnswers = $data["correct-answer"]; 
            $noOfSubQuestions = $request->numofsubq;
            //unset($data['correct_answers']);
            $noofanswers = count($correctAnswers);
            $data["noofanswers"] = $noofanswers;
            $data["noofoptions"] = $data["numofsubq"];

            $parentId = null;
            $subquestions = $data["subquestions"];
            $numberOfAnswers = $data["number-of-answers"];
            // $subanswers = $data["subanswers"];
            // $subquestions = $data["subquestions"];
            unset($data["number-of-answers"]);
            unset($data["subquestions"]);
            unset($data["answers"]);
            unset($data["numofsubq"]);
            unset($data["correct-answer"]);

            // dd($data);
            $this->deleteAnswers($id);
            try{
                $question = Question::where('id' , $id);
                $questionUpdate = $question->update($data);
                $parentId = $id;
                $parentSetId = $question->get()->first()->set_id;
            }
            catch(\Exception $e){
                dd($e);
                return back()->withInput()->with('error','Question could not be added.');
            }
            // dd($id);
            /*subquestions and subanswers*/
            try{
                $createdSubAnswers = [];
                $createdSubQuestions = [];
                for ($i = 1; $i<= count($subquestions) ; $i++) {
                    $data["question"] = $subquestions[$i][0];
                    $data["noofoptions"] =  $numberOfAnswers[$i];
                    $data["noofanswers"] = 1;
                    $data["questiontype_id"] = 1; // Reserved types for subquestions
                    $data["set_id"] = $parentSetId;
                    $answersForSQ = $answers[$i];
                    $question = Question::create($data);
                    for( $j = 0 ; $j < count($answersForSQ ) ; $j++)
                    {
                        $answer = Answer::create([
                            "answer"=>  $answersForSQ[$j],
                            "question_id"=> $question->id,
                        ]);
                        $createdSubAnswers[$i][] = $answer->id;
                    }
                    $createdSubQuestions[$i] = $question->id;
                    SelfReferenceQuestion::create([
                        "question_id" => $question->id,
                        "parent_question_id" => $parentId ,
                    ]);
                }
                for($i = 1 ; $i<= count($correctAnswers); $i++ ) {
                    Rightanswer::create([
                        "answer_id"=>  $createdSubAnswers[$i][(int)$correctAnswers[$i] -1],
                        "question_id"=> $createdSubQuestions[$i],
                        "dropdown_number" => $correctAnswers[$i],

                    ]);
                }
            }
            catch(\Exception $e){
                dd($e);
                return back()->withInput()->with('error','Question could not be added.');
            }
            /*subquestions and subanswers*/

        return redirect()->route('admin.sets.show', $parentSetId)->with('success','Question added successfully.');

        }
        return redirect()->route('admin.sets.show', $question->first()->set_id)->with('success','Question added successfully.');
    }
    public function destroy( $id)
    {
        $findQuestion = Question::find( $id);
        if($findQuestion->questiontype_id == self::MCQ)
        {
            if($findQuestion->delete()){
                return redirect()->back()->with('success', 'Question deleted successfully');
            }
        }
        elseif ($findQuestion->questiontype_id == self::MTF ) {
            $childQuestions = $this->getChildrenQuestions($id)->pluck( "id" )->all();
            try{
                Question::destroy($childQuestions);
                $findQuestion->delete();
                return redirect()->back()->with('success', 'Question deleted successfully');
            }
            catch(\Exception $e){
                dd($e);
            }
        }
        elseif ($findQuestion->questiontype_id == self::QWSQ ) {
            $childQuestions = $this->getChildrenQuestions($id)->pluck( "id" )->all();
            try{
                Question::destroy($childQuestions);
                $findQuestion->delete();
                return redirect()->back()->with('success', 'Question deleted successfully');
            }
            catch(\Exception $e){
                dd($e);
            }
        }
        return back()->withInput()->with('error' , 'Question could not be deleted');
    }
    public function deleteAnswers($id)
    {
        $question = Question::find($id);
        if($question->questiontype_id == self::MCQ)
        {
            $answers = $question->answers()->get(['id']);
            $rightAnswers = $question->rightanswers()->get(['id']);
            try{
                Answer::destroy($answers->toArray());
                Rightanswer::destroy($rightAnswers->toArray());
            }
            catch(\Exception $e){
                dd($e);
            }
        }
        elseif( $question->questiontype_id == self::MTF || $question->questiontype_id == self::QWSQ )
        {
            $answers = $this->getChildrenAnswers($id)->pluck( "id" )->all();
            $rightAnswers = $this->getChildrenRightAnswersId( $id );
            $childQuestions = $this->getChildrenQuestions($id)->pluck( "id" )->all();
            try{
                Answer::destroy($answers);
                Rightanswer::destroy($rightAnswers);
                Question::destroy($childQuestions);
            }
            catch(\Exception $e){
                dd($e);
            }
        }
    }
    function getChildrenQuestions($questionId)
    {
        $childQuestions = SelfReferenceQuestion::where("parent_question_id" , "=" , $questionId )->pluck( "question_id" )->all();
        return Question::whereIn( 'id' , $childQuestions)->get();
    }
    function getChildrenAnswers($questionId)
    {
        $childQuestions = SelfReferenceQuestion::where("parent_question_id" , "=" , $questionId )->pluck( "question_id" )->all();
         $childAnswers = Answer::whereIn("question_id" , $childQuestions )->get();
        return $childAnswers;
    }
    function getChildrenAnswersForQWSQ($questionId)
    {
        $childAnswers = [];
        $childQuestions = SelfReferenceQuestion::where("parent_question_id" , "=" , $questionId )->pluck( "question_id" )->all();
        foreach ($childQuestions as $key => $childQuestion) {
            $childAnswers[$childQuestion] = Answer::where("question_id" , $childQuestion )->get();
        }
        return $childAnswers;
    }
    function getChildrenRightAnswers($questionId)
    {
        $childQuestions = SelfReferenceQuestion::where("parent_question_id" , "=" , $questionId )->pluck( "question_id" )->all();
        $childRightAnswers = Rightanswer::whereIn("question_id"  , $childQuestions )->pluck( "answer_id" )->all();
        return Answer::whereIn( 'id' , $childRightAnswers)->get();
    }
    function getChildrenRightAnswersId($questionId)
    {
        $childQuestions = SelfReferenceQuestion::where("parent_question_id" , "=" , $questionId )->pluck( "question_id" )->all();
        $childRightAnswers = Rightanswer::whereIn("question_id"  , $childQuestions )->pluck( "id" )->all();
        return $childRightAnswers;
    }
    function getChildrenRightAnswersDropdown($questionId)
    {
        $childQuestions = SelfReferenceQuestion::where("parent_question_id" , "=" , $questionId )->pluck( "question_id" )->all();
        $childRightAnswersDropdowns = Rightanswer::whereIn("question_id"  , $childQuestions )->pluck( "dropdown_number" )->all();
        return $childRightAnswersDropdowns;
    }
}
