<?php

namespace App\Http\Controllers\admin;

use App\Subject;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SubjectsController extends  \App\Http\Controllers\Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $subjects = Subject::all();
        return view('admin.subjects.index' , ['subjects'=>$subjects]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try{
            Subject::create($request->except('_token'));
        }
        catch(\Exception $e){
            return back()->withInput()->with('error' , 'Subjects could not be added');
        }
        return back()->with('success' , 'Subjects adding successful.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Subject  $subject
     * @return \Illuminate\Http\Response
     */
    public function show(Subject $subject)
    {
        $sets = Subject::find($subject->id)->sets;
        // dd($sets);
        return view('admin.subjects.show' , ['subject'=> $subject , 'sets'=>$sets ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Subject  $subject
     * @return \Illuminate\Http\Response
     */
    public function edit(Subject $subject)
    {
        return view('admin.subjects.edit' ,['subject'=> $subject ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Subject  $subject
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Subject $subject)
    {
        $subjectUpdate = subject::where('id' , $subject->id)->update($request->except(['_token' , '_method']) );
        if($subjectUpdate){
            return redirect()->route('admin.subjects.index')->with('success', 'subject updated successfully');
        }
        //redirect
        return back()->withInput();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Subject  $subject
     * @return \Illuminate\Http\Response
     */
    public function destroy( $id)
    {
        //
        $findSubject = Subject::find( $id);
        if($findSubject->delete()){
            return redirect()->route('admin.subjects.index')->with('success', 'Subject deleted successfully');
        }
        return back()->withInput()->with('error' , 'Subject could not be deleted');
    }
}
