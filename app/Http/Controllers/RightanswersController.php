<?php

namespace App\Http\Controllers;

use App\Rightanswer;
use Illuminate\Http\Request;

class RightanswersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Rightanswer  $rightanswer
     * @return \Illuminate\Http\Response
     */
    public function show(Rightanswer $rightanswer)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Rightanswer  $rightanswer
     * @return \Illuminate\Http\Response
     */
    public function edit(Rightanswer $rightanswer)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Rightanswer  $rightanswer
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Rightanswer $rightanswer)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Rightanswer  $rightanswer
     * @return \Illuminate\Http\Response
     */
    public function destroy(Rightanswer $rightanswer)
    {
        //
    }
}
