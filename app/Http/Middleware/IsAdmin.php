<?php

namespace App\Http\Middleware;

use Illuminate\Support\Facades\Auth;
use Closure;

class IsAdmin
{
    public function handle($request, Closure $next)
    {
        if(Auth::check()){
            if (Auth::user()->permission == 'admin') {
                return $next($request);
            }
        }
        return redirect()->route('admin.login'); // If user is not an admin.
    }
}