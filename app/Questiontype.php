<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Questiontype extends Model
{
    protected $fillable = [
        'name'	,'noofanswers'
    ];
}
